<form action="{{ route('blogInsert') }}" method="post" class="m-form form-send" autocomplete="off">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Kategori Blog</label>
                            <select class="form-control" name="id_blog_category">
                                @foreach($blog_category as $row)
                                    <option value="{{ $row->id_blog_category }}">{{ $row->blc_judul }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Judul Blog</label>
                            <input type="text" class="form-control m-input" name="blg_judul" autocomplete="new-username"
                                   autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Gambar Blog</label>
                            <input type="file" class="form-control m-input image-browse" name="gambar">
                            <img src="" class="img-responsive image-preview">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Deskripsi Blog</label>
                            <textarea class="form-control summernote" name="blg_isi"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-simpan btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>