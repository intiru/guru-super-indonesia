<form action="{{ route('blogCategoryUpdate', ['id'=>Main::encrypt($edit->id_blog_category)]) }}" method="post" class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Kategori Blog</label>
                            <input type="text" class="form-control m-input" name="blc_judul" value="{{ $edit->blc_judul }}" autocomplete="new-username"
                                   autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Deskripsi Kategori Blog</label>
                            <textarea class="form-control summernote" name="blc_isi">{{ $edit->blc_isi }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>