<form action="{{ route('backFeedAdminUpdate', ['id_feed'=>Main::encrypt($edit->id_feed)]) }}" method="post"
      class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Feed</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Judul Feed</label>
                            <input type="text" class="form-control m-input" name="judul" value="{{ $edit->fed_judul }}"
                                   autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Isi Feed</label>
                            <input type="text" class="form-control m-input" name="isi" value="{{ $edit->fed_isi }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Gambar</label><br/>
                            <label class="btn m-btn--pill text-white btn-primary" style="margin-top: 8px;">
                                <i class="far fa-image"></i> Pilih Gambar
                                <input type="file" hidden name="gambar" class="image-browse">
                            </label>
                            <br/>
                            <img src="{{ asset('upload/'.$edit->fed_gambar) }}" class="image-preview img-fluid">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>