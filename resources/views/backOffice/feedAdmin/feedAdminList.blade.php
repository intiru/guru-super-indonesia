@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/feed.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="row">
                <div class="col-sm-12 col-lg-6 offset-lg-3">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-portlet__body">
                            <form action="{{ route('backFeedAdminPost') }}"
                                  method="post"
                                  class="form-send m-form m-form--fit m-form--label-align-right">

                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-2">
                                        <img src="{{ asset('images/mario_teguh.jpg') }}" class="img-fluid"
                                             style="border-radius: 50%">
                                    </div>
                                    <div class="col-10">
                                        <div class="form-group m-form__group">
                                            <input type="text" class="form-control" name="judul"
                                                   placeholder="Judul Feed ..." style="margin-bottom: 6px">
                                        </div>
                                        <div class="form-group m-form__group">
                                            <textarea class="form-control" name="isi" rows="4"
                                                      placeholder="Isi Feed ..."></textarea>
                                        </div>
                                        <div class="text-right" style="margin-top: 10px">
                                            <label class="btn m-btn--pill btn-primary" style="margin-top: 8px;">
                                                <i class="far fa-image"></i> Pilih Gambar
                                                <input type="file" hidden name="gambar" class="image-browse">
                                            </label>
                                            <button type="submit" class="btn m-btn--pill btn-info">
                                                <i class="fab fa-telegram-plane"></i> Post Status
                                            </button>
                                        </div>
                                        <img src="" class="image-preview img-fluid">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    @foreach($data as $row)
                        @php
                            $id_feed = Main::encrypt($row->id_feed);
                        @endphp
                        <div class="m-portlet m-portlet--mobile akses-list">
                            <div class="m-portlet__body" style="padding-left: 14px; padding-right: 14px">
                                <div class="row feed-item">
                                    <div class="col-sm-2 col-md-2 col-lg-2 ">
                                        <img src="{{ asset('images/mario_teguh.jpg') }}" class="img-fluid"
                                             style="border-radius: 50%">
                                    </div>
                                    <div class=" col-sm-7 col-md-8 col-lg-8">
                                        <h4>Mario Teguh</h4>
                                        <i class="fas fa-clock"></i> {{ $row->created_at }}
                                        <div class="feed-status">
                                            @if($row->fed_status == 'on')
                                                <span class="m-badge m-badge--success m-badge--wide">
                                                    <i class="fas fa-eye"></i> Publish
                                                </span>
                                            @else
                                                <span class="m-badge m-badge--warning m-badge--wide">
                                                    <i class="fas fa-eye-slash"></i> Private
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-2 col-lg-2 feed-menu">
                                        <div class="dropdown">
                                            <button class="btn btn-success btn-sm dropdown-toggle btn-block"
                                                    type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                Menu
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item btn-feed-edit"
                                                   href="#"
                                                   data-route="{{ route('backFeedAdminEditModal', ['id_feed' => $id_feed]) }}">
                                                    <i class="fas fa-pencil-alt"></i> Edit Feed
                                                </a>
                                                @if($row->fed_status == 'on')
                                                    <a class="dropdown-item"
                                                       href="{{ route('backFeedAdminStatus', ['id_feed' => $id_feed, 'fed_status' => 'off' ]) }}">
                                                        <i class="fas fa-eye-slash"></i> Set to Pivate
                                                    </a>
                                                @else
                                                    <a class="dropdown-item"
                                                       href="{{ route('backFeedAdminStatus', ['id_feed' => $id_feed, 'fed_status' => 'on' ]) }}">
                                                        <i class="fas fa-eye"></i> Publish it!
                                                    </a>
                                                @endif
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item btn-feed-delete"
                                                   href="javascript;"
                                                   data-route="{{ route('backFeedAdminDelete', ['id_guru' => Main::encrypt($row->id_feed)]) }}">
                                                    <i class="fas fa-ban"></i> Hapus Feed
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pt-4"
                                         style="padding-right: 15px; padding-left: 15px; margin-bottom: 14px; width: 100%">
                                        <h5 class="card-title">{{ $row->fed_judul }}</h5>
                                        <p class="card-text">
                                            {{ $row->fed_isi }}
                                        </p>
                                    </div>
                                    <div class="text-center" style="text-align: center">
                                        @if($row->fed_gambar)
                                            <img src="{{ asset('upload/'.$row->fed_gambar) }}" class="img-fluid">
                                        @endif
                                    </div>
                                    <div class="card-body" style="padding: 20px">
                                        <div class="m--pull-left">
                                            <i class="fas fa-thumbs-up"></i>
                                            <span style="font-size: 14px">{{ Main::format_number($row->fed_react_like_count) }}</span>
                                        </div>
                                        <div class="m--pull-right" style="font-size: 14px">
                                            <i class="fas fa-comments"></i>
                                            {{ Main::format_number($row->fed_comment_count) }} Komentar
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr style="margin: 20px 0">
                                        <div class="row feed-wrapper">
                                            <div class="col-1">
                                                <img class="img-fluid"
                                                     src="http://localhost/intiru_guru_super_indonesia/public/images/mahendra.png"
                                                     alt="">
                                            </div>
                                            <div class="col-11">
                                                <h6 class="feed-user">Mahendra Wardana 1</h6>
                                                <p class="feed-comment">These utility classes float an element to the
                                                    left or
                                                    right, or disable floating, based on the current viewport sizeThese
                                                    utility
                                                    classes float an element to the left or right, or disable floating,
                                                    based on
                                                    the current viewport size</p>
                                                <div class="feed-action">
                                                    <a href="" class="m--hide">
                                                        <i class="fas fa-thumbs-up"></i> Like
                                                    </a>
                                                    <a href="#" class="btn-feed-reply">
                                                        <i class="fas fa-comment"></i> Balas Komentar
                                                    </a>
                                                    <span class="feed-reply-time">
                                                    42 menit yang lalu
                                                </span>
                                                </div>

                                                <div class="feed-comment-view"></div>

                                            </div>
                                        </div>
                                        <div class="row feed-wrapper">
                                            <div class="col-1 offset-1">
                                                <img class="img-fluid"
                                                     src="http://localhost/intiru_guru_super_indonesia/public/images/mahendra.png"
                                                     alt="">
                                            </div>
                                            <div class="col-10">
                                                <h6 class="feed-user">Mahendra Wardana
                                                    2</h6>
                                                <p style="font-size: 14px">These utility classes float an element to the
                                                    left or
                                                    right, or disable floating, based on the current viewport sizeThese
                                                    utility
                                                    classes float an element to the left or right, or disable floating,
                                                    based on
                                                    the current viewport size</p>

                                                <div class="feed-action">
                                                    <a href="" class="m--hide">
                                                        <i class="fas fa-thumbs-up"></i> Like
                                                    </a>
                                                    <a href="#" class="btn-feed-reply">
                                                        <i class="fas fa-comment"></i> Balas Komentar
                                                    </a>
                                                    <span class="feed-reply-time">
                                                    42 menit yang lalu
                                                </span>
                                                </div>

                                                <div class="feed-comment-view"></div>
                                            </div>
                                        </div>
                                        <div class="row feed-wrapper">
                                            <div class="col-1 offset-2">
                                                <img class="img-fluid"
                                                     src="http://localhost/intiru_guru_super_indonesia/public/images/mahendra.png"
                                                     alt="">
                                            </div>
                                            <div class="col-9">
                                                <h6 class="feed-user">Mahendra Wardana
                                                    3</h6>
                                                <p style="font-size: 14px">These utility classes float an element to the
                                                    left or
                                                    right, or disable floating, based on the current viewport sizeThese
                                                    utility
                                                    classes float an element to the left or right, or disable floating,
                                                    based on
                                                    the current viewport size</p>

                                                <div class="feed-action">
                                                    <a href="" class="m--hide">
                                                        <i class="fas fa-thumbs-up"></i> Like
                                                    </a>
                                                    <a href="#" class="btn-feed-reply">
                                                        <i class="fas fa-comment"></i> Balas Komentar
                                                    </a>
                                                    <span class="feed-reply-time">
                                                    42 menit yang lalu
                                                </span>
                                                </div>

                                                <div class="feed-comment-view"></div>
                                            </div>
                                        </div>
                                        <div class="row feed-wrapper">
                                            <div class="col-1 offset-3">
                                                <img class="img-fluid"
                                                     src="http://localhost/intiru_guru_super_indonesia/public/images/mahendra.png"
                                                     alt="">
                                            </div>
                                            <div class="col-8">
                                                <h6 class="feed-user">Mahendra Wardana
                                                    3</h6>
                                                <p style="font-size: 14px">These utility classes float an element to the
                                                    left or
                                                    right, or disable floating, based on the current viewport sizeThese
                                                    utility
                                                    classes float an element to the left or right, or disable floating,
                                                    based on
                                                    the current viewport size</p>

                                                <div class="feed-action">
                                                    <a href="" class="m--hide">
                                                        <i class="fas fa-thumbs-up"></i> Like
                                                    </a>
                                                    <a href="#" class="btn-feed-reply">
                                                        <i class="fas fa-comment"></i> Balas Komentar
                                                    </a>
                                                    <span class="feed-reply-time">
                                                    42 menit yang lalu
                                                </span>
                                                </div>

                                                <div class="feed-comment-view"></div>
                                            </div>
                                        </div>
                                        <div class="row feed-wrapper">
                                            <div class="col-1 offset-4">
                                                <img class="img-fluid"
                                                     src="http://localhost/intiru_guru_super_indonesia/public/images/mahendra.png"
                                                     alt="">
                                            </div>
                                            <div class="col-7">
                                                <h6 class="feed-user">Mahendra Wardana
                                                    3</h6>
                                                <p style="font-size: 14px">These utility classes float an element to the
                                                    left or
                                                    right, or disable floating, based on the current viewport sizeThese
                                                    utility
                                                    classes float an element to the left or right, or disable floating,
                                                    based on
                                                    the current viewport size</p>

                                                <div class="feed-action">
                                                    <a href="" class="m--hide">
                                                        <i class="fas fa-thumbs-up"></i> Like
                                                    </a>
                                                    <a href="#" class="btn-feed-reply">
                                                        <i class="fas fa-comment"></i> Balas Komentar
                                                    </a>
                                                    <span class="feed-reply-time">
                                                    42 menit yang lalu
                                                </span>
                                                </div>

                                                <div class="feed-comment-view"></div>
                                            </div>
                                        </div>
                                        <div class="row feed-wrapper">
                                            <div class="col-1 offset-1">
                                                <img class="img-fluid"
                                                     src="http://localhost/intiru_guru_super_indonesia/public/images/mahendra.png"
                                                     alt="">
                                            </div>
                                            <div class="col-10">
                                                <h6 class="feed-user">Mahendra Wardana
                                                    4</h6>
                                                <p style="font-size: 14px">These utility classes float an element to the
                                                    left or
                                                    right, or disable floating, based on the current viewport sizeThese
                                                    utility
                                                    classes float an element to the left or right, or disable floating,
                                                    based on
                                                    the current viewport size</p>

                                                <div class="feed-action">
                                                    <a href="" class="m--hide">
                                                        <i class="fas fa-thumbs-up"></i> Like
                                                    </a>
                                                    <a href="#" class="btn-feed-reply">
                                                        <i class="fas fa-comment"></i> Balas Komentar
                                                    </a>
                                                    <span class="feed-reply-time">
                                                    42 menit yang lalu
                                                </span>
                                                </div>

                                                <div class="feed-comment-view"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>

    <div class="feed-comment-html m--hide">
        <textarea id="textInputExample" type="text" class="form-control feed-input"
                  placeholder="Ketik komentar Anda ..."></textarea>
        <div class="text-right feed-btn-balas-komentar">
            <button class="btn m-btn--pill btn-info btn-sm">
                <i class="fab fa-telegram-plane"></i> Kirim Komentar
            </button>
            <button class="btn m-btn--pill btn-default btn-sm btn-feed-comment-cancel">
                <i class="fab fa-close"></i> Batal
            </button>
        </div>
    </div>
@endsection
