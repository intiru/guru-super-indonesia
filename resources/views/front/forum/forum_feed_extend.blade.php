<div class="forum-comment-html d-none">
    <div class="row">
        <div class="col-1">
            <img class="avatar w-8" src="{{ hForum::guru_avatar_forum($front_guru->gru_avatar) }}" alt=""/>
        </div>
        <div class="col-11">
            <form class="form-send" action="{{ route('forumGuruComment') }}" method="post">
                <input type="hidden" name="id_feed">
                <input type="hidden" name="id_feed_comment_parent">
                {{ csrf_field() }}
                <div class="form-floating">
                    <input type="text" name="isi" class="form-control feed-comment-input"
                           placeholder="Ketik komentar Anda ...">
                    <label for="textInputExample">Ketik komentar Anda ...</label>
                    <div class="text-end feed-comment-reply-button">
                        <button type="button" class="btn-feed-comment-cancel btn btn-soft-ash rounded-pill btn-sm ">
                            Batal
                        </button>
                        <button type="submit" class="btn btn-blue rounded-pill btn-sm"><i
                                class="uil uil-navigator"></i> Kirim Komentar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="forum-comment-edit-html d-none">
    <form class="form-send" action="{{ route('forumGuruCommentUpdate') }}" method="post">
        <input type="hidden" name="id_feed_comment">
        {{ csrf_field() }}
        <div class="form-floating">
            <input type="text" name="isi" class="form-control feed-comment-input"
                   placeholder="Ketik komentar Anda ...">
            <label for="textInputExample">Ketik komentar Anda ...</label>
            <div class="text-end feed-comment-reply-button">
                <button type="button" class="btn btn-soft-ash btn-feed-comment-edit-cancel rounded-pill btn-sm">
                    Batal
                </button>
                <button type="submit" class="btn btn-blue rounded-pill btn-sm"><i
                        class="uil uil-navigator"></i> Perbarui Komentar
                </button>
            </div>
        </div>
    </form>
</div>