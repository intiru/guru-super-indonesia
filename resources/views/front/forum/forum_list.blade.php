@extends('front.front.index')

@section('content')

    <section class="wrapper bg-light angled upper-end  pt-8 pb-15 pt-md-14">
        <div class="container">
            <div class="row mb-4">
                <div class="col-lg-9 col-xl-9">
                    <div class="card mb-4">
                        <div class="card-body" style="padding: 20px">
                            <form action="{{ route('forumGuruFeedCreate') }}" method="post" class="form-send">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-2">
                                        <img class="avatar w-100"
                                             src="{{ \app\Helpers\hForum::guru_avatar_forum($front_guru->gru_avatar) }}"
                                             alt=""/>
                                    </div>
                                    <div class="col-10">
                                        <div class="form-floating">
                                        <textarea id="textInputExample" type="text" class="form-control"
                                                  placeholder="Ketik komentar Anda ..."
                                                  style="height: 120px; max-height: 200px" name="feed"></textarea>
                                            <label for="textInputExample">Ketik komentar Anda ...</label>
                                            <div class="float-end mt-2">
                                                <label class="btn btn-soft-sky rounded-pill btn-sm"
                                                       style="margin-top: 8px;">
                                                    <i class="uil uil-image"></i> Pilih Gambar
                                                    <input type="file" hidden name="gambar" class="image-browse">
                                                </label>
                                                <button type="submit" class="btn btn-blue rounded-pill btn-sm"><i
                                                            class="uil uil-navigator"></i> Post Status
                                                </button>
                                            </div>
                                            <div class="clearfix"></div>
                                            <br/>
                                            <img src="" class="image-preview img-fluid">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @foreach($feed_list as $row)
                        @php
                            $id_feed = Main::encrypt($row->id_feed);
                            $liked_status_check =
                            \app\Models\mFeedReact
                                ::where([
                                    'id_feed' => $row->id_feed,
                                    'id_guru' => $front_guru->id_guru
                                ])
                                ->count();
                            if($liked_status_check > 0) {
                                $liked_status = 'feed-liked';
                            } else {
                                $liked_status = '';
                            }

                        @endphp
                        <div class="card mb-4 feed-wrapper" data-id-feed="{{ $id_feed }}">
                            <div class="card-body" style="padding: 20px">
                                <div class="float-start">
                                    <img class="avatar w-12" src="{{ \app\Helpers\hForum::forum_feed_avatar($row) }}"
                                         alt=""/>
                                </div>
                                <div class="float-start" style="margin-top: 0px; padding-left: 10px">
                                    <div style="color: black; font-weight: bold">{{ \app\Helpers\hForum::forum_feed_name($row) }}</div>
                                    <div style="font-weight: lighter; font-size: 12px"><i class="uil uil-clock"></i>
                                        {{ $row->updated_at }}
                                    </div>
                                </div>
                                <div class="float-end feed-options">
                                    {!! \app\Helpers\hForum::forum_feed_options($row, $id_feed) !!}
                                </div>
                                <div class="clearfix"></div>
                                <div class="pt-4">
                                    <h5 class="card-title">{{ $row->fed_judul }}</h5>
                                    <p class="card-text">
                                        {{ $row->fed_isi }}
                                    </p>
                                </div>
                            </div>
                            @if($row->fed_gambar)
                                <img class="card-img-bottom" src="{{ asset('upload/'.$row->fed_gambar) }}" alt=""/>
                            @endif
                            <div class="card-body" style="padding: 20px">
                                <div class="float-start">
                                    <i class="uil uil-thumbs-up"></i>
                                    {{--                                        <i class="uil uil-smile"></i>--}}
                                    <span style="font-size: 14px"
                                          class="feed-react-total">{{ Main::format_number($row->fed_react_like_count) }}</span>
                                </div>
                                <div class="float-end" style="font-size: 14px">
                                    {{ Main::format_number($row->fed_comment_count) }} Komentar
                                </div>
                                <div class="clearfix"></div>
                                <hr class="my-3"/>
                                <div class="row" style="padding-top: 0; padding-bottom: 0">
                                    <div class="col-6 text-center btn-forum-feed-like {{ $liked_status }}">
                                        <a href="#" class="btn-forum-like">
                                            <i class="uil uil-thumbs-up"></i> Suka
                                        </a>
                                    </div>
                                    <div class="col-6 text-center"
                                         data-id-feed-comment-parent="{{ Main::encrypt(0) }}">
                                        <a href="#" class=" btn-forum-comment-first">
                                            <i class="uil uil-comment"></i> Komentar
                                        </a>
                                    </div>
                                </div>
                                <hr class="my-3"/>

                                <div class="feed-comment-wrapper">
                                    @foreach($row->feed_comment as $row_comment)
                                        @php
                                            $id_feed_comment = Main::encrypt($row_comment->id_feed_comment);
                                        @endphp
                                        <div class="row" data-id-feed-comment="{{ $id_feed_comment }}">
                                            <div class="col-1">
                                                <img class="avatar w-8"
                                                     src="{{ hForum::guru_avatar_forum($row_comment->guru->gru_avatar) }}"
                                                     alt=""/>
                                            </div>
                                            <div class="col-11 feed-comment-row">
                                                <div class="float-start">
                                                    <h6 class="feed-comment-nama">{{ $row_comment->guru->gru_nama_depan.' '.$row_comment->guru->gru_nama_belakang }}</h6>
                                                </div>
                                                <div class="float-start feed-comment-options">
                                                    {!! \app\Helpers\hForum::forum_comment_options($row_comment, $id_feed_comment) !!}
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="feed-comment-isi">
                                                    <p>{{ $row_comment->fdc_isi }}</p>
                                                </div>
                                                <div class="feed-comment-menu">
                                                    <span
                                                            class="btn-feed-comment-reply"
                                                            data-id-feed-comment="{{ $id_feed_comment }}">
                                                        <i class="uil uil-comment"></i> Balas Komentar
                                                    </span>
                                                    <span
                                                            class="feed-comment-time">
                                                        <i class="uil uil-clock"></i> {{ $row_comment->updated_at }}
                                                    </span>
                                                </div>
                                                <div class="feed-comment-reply-input"></div>
                                            </div>
                                        </div>
                                        @foreach($row_comment->feed_comment as $row_comment_2)
                                            @php
                                                $id_feed_comment = Main::encrypt($row_comment_2->id_feed_comment);
                                            @endphp
                                            <div class="row" data-id-feed-comment="{{ $id_feed_comment }}">
                                                <div class="col-1 offset-1">
                                                    <img class="avatar w-8"
                                                         src="{{ hForum::guru_avatar_forum($row_comment_2->guru->gru_avatar) }}"
                                                         alt=""/>
                                                </div>
                                                <div class="col-10 feed-comment-row">
                                                    <div class="float-start">
                                                        <h6 class="feed-comment-nama">{{ $row_comment_2->guru->gru_nama_depan.' '.$row_comment_2->guru->gru_nama_belakang }}</h6>
                                                    </div>
                                                    <div class="float-start feed-comment-options">
                                                        {!! \app\Helpers\hForum::forum_comment_options($row_comment_2, $id_feed_comment) !!}
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="feed-comment-isi">
                                                        <p>{{ $row_comment_2->fdc_isi }}</p>
                                                    </div>
                                                    <div class="feed-comment-menu">
                                                        <span
                                                                class="btn-feed-comment-reply"
                                                                data-id-feed-comment="{{ $id_feed_comment }}">
                                                            <i class="uil uil-comment"></i> Balas Komentar
                                                        </span>
                                                        <span
                                                                class="feed-comment-time">
                                                        <i class="uil uil-clock"></i> {{ $row_comment_2->updated_at }}
                                                    </span>
                                                    </div>
                                                    <div class="feed-comment-reply-input"></div>
                                                </div>
                                            </div>
                                            @foreach($row_comment_2->feed_comment as $row_comment_3)
                                                @php
                                                    $id_feed_comment = Main::encrypt($row_comment_3->id_feed_comment);
                                                @endphp
                                                <div class="row" data-id-feed-comment="{{ $id_feed_comment }}">
                                                    <div class="col-1 offset-2">
                                                        <img class="avatar w-8"
                                                             src="{{ hForum::guru_avatar_forum($row_comment_3->guru->gru_avatar) }}"
                                                             alt=""/>
                                                    </div>
                                                    <div class="col-9 feed-comment-row">
                                                        <div class="float-start">
                                                            <h6 class="feed-comment-nama">{{ $row_comment_3->guru->gru_nama_depan.' '.$row_comment_3->guru->gru_nama_belakang }}</h6>
                                                        </div>
                                                        <div class="float-start feed-comment-options">
                                                            {!! \app\Helpers\hForum::forum_comment_options($row_comment_3, $id_feed_comment) !!}
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="feed-comment-isi">
                                                            <p>{{ $row_comment_3->fdc_isi }}</p>
                                                        </div>
                                                        <div class="feed-comment-menu">
                                                            <span
                                                                    class="btn-feed-comment-reply"
                                                                    data-id-feed-comment="{{ $id_feed_comment }}">
                                                                <i class="uil uil-comment"></i> Balas Komentar
                                                            </span>
                                                            <span
                                                                    class="feed-comment-time">
                                                        <i class="uil uil-clock"></i> {{ $row_comment_3->updated_at }}
                                                    </span>
                                                        </div>
                                                        <div class="feed-comment-reply-input"></div>
                                                    </div>
                                                </div>
                                                @foreach($row_comment_3->feed_comment as $row_comment_4)
                                                    @php
                                                        $id_feed_comment = Main::encrypt($row_comment_4->id_feed_comment);
                                                    @endphp
                                                    <div class="row" data-id-feed-comment="{{ $id_feed_comment }}">
                                                        <div class="col-1 offset-3">
                                                            <img class="avatar w-8"
                                                                 src="{{ hForum::guru_avatar_forum($row_comment_4->guru->gru_avatar) }}"
                                                                 alt=""/>
                                                        </div>
                                                        <div class="col-8 feed-comment-row">
                                                            <div class="float-start">
                                                                <h6 class="feed-comment-nama">{{ $row_comment_4->guru->gru_nama_depan.' '.$row_comment_4->guru->gru_nama_belakang }}</h6>
                                                            </div>
                                                            <div class="float-start feed-comment-options">
                                                                {!! \app\Helpers\hForum::forum_comment_options($row_comment_4, $id_feed_comment) !!}
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="feed-comment-isi">
                                                                <p>{{ $row_comment_4->fdc_isi }}</p>
                                                            </div>
                                                            <div class="feed-comment-menu">
                                                                <span
                                                                        class="btn-feed-comment-reply"
                                                                        data-id-feed-comment="{{ $id_feed_comment }}">
                                                                    <i class="uil uil-comment"></i> Balas Komentar
                                                                </span>
                                                                <span
                                                                        class="feed-comment-time">
                                                                    <i class="uil uil-clock"></i> {{ $row_comment_4->updated_at }}
                                                                </span>
                                                            </div>
                                                            <div class="feed-comment-reply-input"></div>
                                                        </div>
                                                    </div>
                                                    @foreach($row_comment_4->feed_comment as $row_comment_5)
                                                        @php
                                                            $id_feed_comment = Main::encrypt($row_comment_5->id_feed_comment);
                                                        @endphp
                                                        <div class="row" data-id-feed-comment="{{ $id_feed_comment }}">
                                                            <div class="col-1 offset-4">
                                                                <img class="avatar w-8"
                                                                     src="{{ hForum::guru_avatar_forum($row_comment_5->guru->gru_avatar) }}"
                                                                     alt=""/>
                                                            </div>
                                                            <div class="col-7 feed-comment-row">
                                                                <div class="float-start">
                                                                    <h6 class="feed-comment-nama">{{ $row_comment_5->guru->gru_nama_depan.' '.$row_comment_5->guru->gru_nama_belakang }}</h6>
                                                                </div>
                                                                <div class="float-start feed-comment-options">
                                                                    {!! \app\Helpers\hForum::forum_comment_options($row_comment_5, $id_feed_comment) !!}
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="feed-comment-isi">
                                                                    <p>{{ $row_comment_5->fdc_isi }}</p>
                                                                </div>
                                                                <div class="feed-comment-menu">
                                                                    <span
                                                                            class="btn-feed-comment-reply"
                                                                            data-id-feed-comment="{{ $id_feed_comment }}">
                                                                        <i class="uil uil-comment"></i> Balas Komentar
                                                                    </span>
                                                                    <span
                                                                            class="feed-comment-time">
                                                                        <i class="uil uil-clock"></i> {{ $row_comment_5->updated_at }}
                                                                    </span>
                                                                </div>
                                                                <div class="feed-comment-reply-input"></div>
                                                            </div>
                                                        </div>
                                                        @foreach($row_comment_5->feed_comment as $row_comment_6)
                                                            @php
                                                                $id_feed_comment = Main::encrypt($row_comment_6->id_feed_comment);
                                                            @endphp
                                                            <div class="row"
                                                                 data-id-feed-comment="{{ $id_feed_comment }}">
                                                                <div class="col-1 offset-5">
                                                                    <img class="avatar w-8"
                                                                         src="{{ hForum::guru_avatar_forum($row_comment_6->guru->gru_avatar) }}"
                                                                         alt=""/>
                                                                </div>
                                                                <div class="col-6 feed-comment-row">
                                                                    <div class="float-start">
                                                                        <h6 class="feed-comment-nama">{{ $row_comment_6->guru->gru_nama_depan.' '.$row_comment_6->guru->gru_nama_belakang }}</h6>
                                                                    </div>
                                                                    <div class="float-start feed-comment-options">
                                                                        {!! \app\Helpers\hForum::forum_comment_options($row_comment_6, $id_feed_comment) !!}
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="feed-comment-isi">
                                                                        <p>{{ $row_comment_6->fdc_isi }}</p>
                                                                    </div>
                                                                    <div class="feed-comment-menu">
                                                                        <span
                                                                                class="btn-feed-comment-reply"
                                                                                data-id-feed-comment="{{ $id_feed_comment }}">
                                                                            <i class="uil uil-comment"></i> Balas Komentar
                                                                        </span>
                                                                        <span
                                                                                class="feed-comment-time">
                                                                            <i class="uil uil-clock"></i> {{ $row_comment_6->updated_at }}
                                                                        </span>
                                                                    </div>
                                                                    <div class="feed-comment-reply-input"></div>
                                                                </div>
                                                            </div>
                                                            @foreach($row_comment_6->feed_comment as $row_comment_7)
                                                                @php
                                                                    $id_feed_comment = Main::encrypt($row_comment_7->id_feed_comment);
                                                                @endphp
                                                                <div class="row"
                                                                     data-id-feed-comment="{{ $id_feed_comment }}">
                                                                    <div class="col-1 offset-6">
                                                                        <img class="avatar w-8"
                                                                             src="{{ hForum::guru_avatar_forum($row_comment_7->guru->gru_avatar) }}"
                                                                             alt=""/>
                                                                    </div>
                                                                    <div class="col-5 feed-comment-row">
                                                                        <div class="float-start">
                                                                            <h6 class="feed-comment-nama">{{ $row_comment_7->guru->gru_nama_depan.' '.$row_comment_7->guru->gru_nama_belakang }}</h6>
                                                                        </div>
                                                                        <div class="float-start feed-comment-options">
                                                                            {!! \app\Helpers\hForum::forum_comment_options($row_comment_7, $id_feed_comment) !!}
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="feed-comment-isi">
                                                                            <p>{{ $row_comment_7->fdc_isi }}</p>
                                                                        </div>
                                                                        <div class="feed-comment-menu">
                                                                            <span
                                                                                    class="feed-comment-time">
                                                                                <i class="uil uil-clock"></i> {{ $row_comment_7->updated_at }}
                                                                            </span>
                                                                        </div>
                                                                        {{--                                                                        <div class="feed-comment-menu">--}}
                                                                        {{--                                                                            <span--}}
                                                                        {{--                                                                                    class="btn-feed-comment-reply"--}}
                                                                        {{--                                                                                    data-id-feed-comment="{{ $id_feed_comment }}">--}}
                                                                        {{--                                                                                <i class="uil uil-comment"></i> Balas Komentar--}}
                                                                        {{--                                                                            </span>--}}
                                                                        {{--                                                                        </div>--}}
                                                                        {{--                                                                        <div class="feed-comment-reply-input"></div>--}}
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                </div>

                                <div class="row">
                                    <div class="col-1">
                                        <img class="avatar w-8"
                                             src="{{ hForum::guru_avatar_forum($front_guru->gru_avatar) }}"
                                             alt=""/>
                                    </div>
                                    <div class="col-11">
                                        <form class="form-send" action="{{ route('forumGuruComment') }}" method="post">
                                            <input type="hidden" name="id_feed" value="{{ $id_feed }}">
                                            <input type="hidden" name="id_feed_comment_parent"
                                                   value="{{ Main::encrypt(0) }}">
                                            {{ csrf_field() }}
                                            <div class="form-floating">
                                                <input id="textInputExample" type="text" name="isi"
                                                       class="form-control feed-comment-input"
                                                       placeholder="Ketik komentar Anda ...">
                                                <label for="textInputExample">Ketik komentar Anda ...</label>
                                                <div class="text-end feed-comment-reply-button">
                                                    <button type="submit" class="btn btn-blue rounded-pill btn-sm"><i
                                                                class="uil uil-navigator"></i> Kirim Komentar
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach


                    {{--                    <div class="text-center">--}}
                    {{--                        <button type="button" class="btn btn-blue rounded-pill"><i class="uil uil-redo"></i> Lihat--}}
                    {{--                            Selanjutnya--}}
                    {{--                        </button>--}}
                    {{--                    </div>--}}
                </div>
                <aside class="col-lg-3 sidebar mt-6">
                    <div class="widget">
                        <h4 class="display-4 mb-1 mt-0">Forum Diskusi</h4>
                        <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum.
                            Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget
                            metus.</p>
                    </div>
                    <!-- /.widget -->
                    <div class="widget">
                        <h4 class="widget-title mb-3"><i class="uil uil-top-arrow-from-top"></i> Diskusi Teratas</h4>
                        <ul class="image-list">
                            @foreach($feed_top as $row)
                                <li>
                                    <figure class="rounded">
                                        @if($row->fed_gambar)
                                            <a href="#">
                                                <img src="{{ asset('upload/'.$row->fed_gambar) }}" alt="">
                                            </a>
                                        @endif
                                    </figure>
                                    <div class="post-content">
                                        <h6 class="mb-2">
                                            <a class="link-dark" href="#">
                                                @if($row->fed_judul)
                                                    {{ $row->fed_judul }}
                                                @else
                                                    {{ substr($row->fed_isi ,0, 100) }} ...
                                                @endif
                                            </a>
                                        </h6>
                                        <ul class="post-meta">
                                            <li class="post-date"><i
                                                        class="uil uil-calendar-alt"></i><span>{{ $row->created_at }}</span></li>
                                            <li class="post-comments"><a href="#"><i
                                                            class="uil uil-comment"></i>{{ Main::format_number($row->fed_comment_count) }}</a></li>
                                        </ul>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </aside>
            </div>
        </div>

    </section>


    @include('front.forum.forum_feed_extend')

@endsection