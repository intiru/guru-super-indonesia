@if($id_guru == $row->id_guru)
    <div class="dropdown">
    <span class="dropdown-toggle" type="button"
          id="dropdownMenuButton1" data-bs-toggle="dropdown"
          aria-expanded="false">
        <i class="uil uil-ellipsis-h"></i>
    </span>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <li><a class="dropdown-item btn-feed-comment-edit" href="#" data-id-feed-comment="{{ $id_feed_comment }}"><i class="uil uil-edit-alt"></i> Edit</a></li>
            <li><a class="dropdown-item btn-feed-comment-delete" href="#" data-id-feed-comment="{{ $id_feed_comment }}"><i class="uil uil-multiply"></i> Hapus</a></li>
        </ul>
    </div>
@endif