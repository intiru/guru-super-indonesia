@extends('front.front.index')

@section('content')

    <section class="wrapper bg-light angled upper-end  pt-8 pb-15 pt-md-14">
        <div class="container">
            <div class="row mb-4">
                <div class="col-lg-6 col-xl-6 offset-lg-1 offset-xl-1">
                    <div class="mb-4">
                        <p><strong>Daftar</strong> atau <strong>Login</strong> Terlebih dahulu untuk masuk kedalam
                            Forum
                            Diskusi <span class="display-4" style="font-size: 20px; color: #3F78E0"> Guru Super Indonesia</span>.
                        </p>
                        <button type="button"
                                class="btn btn-sm btn-primary rounded-pill"
                                data-bs-toggle="modal"
                                data-bs-target="#modal-daftar">
                            <i class="uil uil-user"></i> Daftar Disini
                        </button>
                        <button type="button" class="btn btn-outline-gradient gradient-7 rounded-pill"
                                data-bs-toggle="modal"
                                data-bs-target="#modal-login">
                            <i class="uil uil-check-circle"></i> Login Disini
                        </button>

                    </div>
                    <div class="card" aria-hidden="true">
                        <div class="card-body">
                            <h5 class="card-title placeholder-glow">
                                <span class="placeholder col-6"></span>
                            </h5>
                            <p class="card-text placeholder-glow">
                                <span class="placeholder col-7"></span>
                                <span class="placeholder col-4"></span>
                                <span class="placeholder col-4"></span>
                                <span class="placeholder col-6"></span>
                                <span class="placeholder col-8"></span>
                            </p>
                            <div class="placeholder-glow">
                                <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <aside class="col-lg-3 sidebar mt-6">
                    <div class="widget">
                        <h4 class="display-4 mb-1 mt-0">Forum Diskusi</h4>
                        <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum.
                            Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget
                            metus.</p>
                    </div>
                    <!-- /.widget -->
                    <div class="widget">
                        <h4 class="widget-title mb-3"><i class="uil uil-top-arrow-from-top"></i> Diskusi Teratas</h4>

                        <div class="card" aria-hidden="true">
                            <div class="card-body">
                                <h5 class="card-title placeholder-glow">
                                    <span class="placeholder col-6"></span>
                                </h5>
                                <p class="card-text placeholder-glow">
                                    <span class="placeholder col-7"></span>
                                    <span class="placeholder col-4"></span>
                                    <span class="placeholder col-4"></span>
                                    <span class="placeholder col-6"></span>
                                    <span class="placeholder col-8"></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>

    </section>

@endsection