<div class="modal fade" id="modal-forum-feed-edit-modal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content text-center">
            <div class="modal-body">
                <button class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                <div class="row">
                    <div class="col-2">
                        <img class="avatar w-100"
                             src="{{ \app\Helpers\hForum::guru_avatar_forum($front_guru->gru_avatar) }}" alt=""/>
                    </div>
                    <div class="col-10">
                        <form class="form-send"
                              action="{{ route('forumGuruFeedUpdate', ['id_feed' => Main::encrypt($feed->id_feed)]) }}"
                              method="post">
                            {{ csrf_field() }}
                            <div class="form-floating">
                                <textarea id="textInputExample" type="text" class="form-control"
                                          placeholder="Ketik komentar Anda ..."
                                          style="height: 120px; max-height: 200px"
                                          name="feed">{{ $feed->fed_isi }}</textarea>
                                <label for="textInputExample">Ketik komentar Anda ...</label>
                                <div class="float-end mt-2">
                                    <label class="btn btn-soft-sky rounded-pill btn-sm"
                                           style="margin-top: 8px;">
                                        <i class="uil uil-image"></i> Pilih Gambar
                                        <input type="file" hidden name="gambar" class="image-browse">
                                    </label>
                                    <button type="submit" class="btn btn-blue rounded-pill btn-sm"><i
                                                class="uil uil-navigator"></i> Perbarui Status
                                    </button>
                                </div>
                                <div class="clearfix"></div>
                                <br/>
                                <img src="{{ asset('upload/'.$feed->fed_gambar) }}" class="image-preview img-fluid">
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>