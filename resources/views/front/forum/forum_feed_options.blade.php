@if($id_guru == $feed->id_guru)
    <div class="dropdown">
        <span class="dropdown-toggle" type="button"
                  id="dropdownMenuButton1" data-bs-toggle="dropdown"
                  aria-expanded="false">
                <i class="uil uil-ellipsis-h"></i>
        </span>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <li><a class="dropdown-item btn-forum-feed-edit" href="#" data-id-feed="{{ $id_feed }}"><i class="uil uil-edit-alt"></i> Edit</a></li>
            <li><a class="dropdown-item btn-forum-feed-delete" href="#" data-id-feed="{{ $id_feed }}"><i class="uil uil-multiply"></i> Hapus</a></li>
        </ul>
    </div>
@endif