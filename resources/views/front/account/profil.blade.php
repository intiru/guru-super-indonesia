@extends('front.front.index')

@section('css')
    <link rel="stylesheet" type="text/css"
          href="{{ asset('front/plugin/datepicker/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('front/plugin/datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.datepicker').datepicker({
                format: "dd-mm-yyyy",
                orientation: "bottom left",
                autoclose: true
            });
        });
    </script>
@endsection

@section('content')

    <form class="contact-form needs-validation form-send" method="post" action="{{ route('accountProfilUpdate') }}">
        {{ csrf_field() }}
        <section class="wrapper bg-light angled upper-end  pt-8 pt-md-14">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-xl-3">
                        <img class="avatar w-100 image-preview" src="{{ Main::guru_avatar_path() }}" alt=""/>
                        <div class="col-md-12">
                            <div class="form-floating mb-4">
                                Foto Profil *
                                <input type="file" name="avatar" class="form-control image-browse">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 offset-lg-1 col-xl-8 offset-xl-1">
                        <div class="messages"></div>
                        <div class="row gx-4">
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="nama_depan"
                                           value="{{ $guru->gru_nama_depan }}" class="form-control"
                                           placeholder="Doe">
                                    <label for="form_lastname">Nama Depan *</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="nama_belakang"
                                           value="{{ $guru->gru_nama_belakang }}" class="form-control"
                                           placeholder="Doe">
                                    <label for="form_lastname">Nama Belakang *</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="nama_lengkap"
                                           value="{{ $guru->gru_nama_lengkap }}" class="form-control"
                                           placeholder="Doe">
                                    <label for="form_lastname">Nama Lengkap *</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="tempat_lahir"
                                           value="{{ $guru->gru_tempat_lahir }}" class="form-control"
                                           placeholder="Doe">
                                    <label for="form_lastname">Tempat Lahir *</label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="tanggal_lahir"
                                           value="{{ Main::format_date($guru->gru_tanggal_lahir) }}"
                                           class="form-control datepicker"
                                           placeholder="Doe">
                                    <label for="form_lastname">Tanggal Lahir *</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-4">
                                    <label for="form_lastname" class="mb-2">
                                        <strong>Jenis Kelamin *</strong>
                                    </label>
                                    <br/>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="jenis_kelamin" value="pria"
                                               id="flexRadioDefault1" {{ $guru->gru_jenis_kelamin == 'pria' ? 'checked':'' }}>
                                        <label class="form-check-label" for="flexRadioDefault1"> Pria </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="jenis_kelamin" value="wanita"
                                               id="flexRadioDefault2" {{ $guru->gru_jenis_kelamin == 'wanita' ? 'checked':'' }}>
                                        <label class="form-check-label" for="flexRadioDefault2"> Wanita </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="alamat_kediaman"
                                           value="{{ $guru->gru_alamat_kediaman }}" class="form-control"
                                           placeholder="Doe">
                                    <label for="form_lastname">Alamat Kediaman *</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="alamat_mengajar"
                                           value="{{ $guru->gru_alamat_mengajar }}" class="form-control"
                                           placeholder="Doe">
                                    <label for="form_lastname">Alamat Tempat Mengajar *</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="bidang_pengajaran"
                                           value="{{ $guru->gru_bidang_pengajaran }}" class="form-control"
                                           placeholder="Doe">
                                    <label for="form_lastname">Bidang Pengajaran *</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="kedudukan_pengajaran"
                                           value="{{ $guru->gru_kedudukan_pengajaran }}"
                                           class="form-control" placeholder="Doe">
                                    <label for="form_lastname">Kedudukan di Lembaga Pengajaran *</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="mulai_bekerja"
                                           value="{{ Main::format_date($guru->gru_mulai_bekerja) }}"
                                           class="form-control datepicker"
                                           placeholder="Doe">
                                    <label for="form_lastname">Mulai Bekerja dari Tahun *</label>
                                </div>
                            </div>

                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary rounded-pill">
                                    <i class="uil uil-check"></i>
                                    Perbarui Profil
                                </button>
                                <p class="text-muted"><strong>*</strong> Kolom perlu diisi.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </form>

@endsection