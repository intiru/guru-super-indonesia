@extends('front.account.profil_lengkapi_step_list')

@section('css')
    <link rel="stylesheet" type="text/css"
          href="{{ asset('front/plugin/datepicker/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('front/plugin/datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.datepicker').datepicker({
                format: "dd-mm-yyyy",
                orientation: "bottom left",
                autoclose: true
            });
        });
    </script>
@endsection



@section('content_sub')

    <section class="wrapper bg-light angled upper-end">
        <div class="container pb-11">
            <div class="row">
                <div class="col-lg-5 col-xl-5">
                    <img class="img-fluid" src="{{ asset('front/img/illustrations/i1.png') }}"
                         srcset="{{ asset('front/img/illustrations/i1@2x.png') }} 2x" alt=""/>
                </div>
                <div class="col-lg-6 offset-lg-1 col-xl-6 offset-xl-1">
                    <form class="contact-form needs-validation form-send" method="post" action="{{ route('accountProfilUpdate') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="nama_depan" value="{{ $guru->gru_nama_depan }}">
                        <input type="hidden" name="nama_belakang" value="{{ $guru->gru_nama_belakang }}">

                        <div class="row gx-4">
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <h5>Email :</h5>
                                    {{ $guru->gru_email }}

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <h5>Nama Depan & Belakang :</h5>
                                    {{ $guru->gru_nama_depan.' - '.$guru->gru_nama_belakang }}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="nama_lengkap" class="form-control"
                                           placeholder="Doe">
                                    <label for="form_lastname">Nama Lengkap *</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="tempat_lahir" class="form-control"
                                           placeholder="Doe">
                                    <label for="form_lastname">Tempat Lahir *</label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="tanggal_lahir" class="form-control datepicker"
                                           placeholder="Doe">
                                    <label for="form_lastname">Tanggal Lahir *</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-4">
                                    <label for="form_lastname" class="mb-2">
                                        <strong>Jenis Kelamin *</strong>
                                    </label>
                                    <br/>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="jenis_kelamin" value="pria"
                                               id="flexRadioDefault1" checked>
                                        <label class="form-check-label" for="flexRadioDefault1"> Pria </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="jenis_kelamin" value="wanita"
                                               id="flexRadioDefault2">
                                        <label class="form-check-label" for="flexRadioDefault2"> Wanita </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="alamat_kediaman" class="form-control"
                                           placeholder="Doe">
                                    <label for="form_lastname">Alamat Kediaman *</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="alamat_mengajar" class="form-control"
                                           placeholder="Doe">
                                    <label for="form_lastname">Alamat Tempat Mengajar *</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="bidang_pengajaran" class="form-control"
                                           placeholder="Doe">
                                    <label for="form_lastname">Bidang Pengajaran *</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="kedudukan_pengajaran"
                                           class="form-control" placeholder="Doe">
                                    <label for="form_lastname">Kedudukan di Lembaga Pengajaran *</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="text" name="mulai_bekerja" class="form-control datepicker"
                                           placeholder="Doe">
                                    <label for="form_lastname">Mulai Bekerja dari Tahun *</label>
                                </div>
                            </div>

                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary rounded-pill"><i class="uil uil-check"></i>
                                    Simpan & Langkah Selanjutnya
                                </button>
                                <p class="text-muted"><strong>*</strong> Kolom perlu diisi.</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection