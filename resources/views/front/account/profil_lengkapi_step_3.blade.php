@extends('front.account.profil_lengkapi_step_list')

@section('content_sub')
    <section class="wrapper bg-light angled upper-end">
        <div class="container pb-11">
            <div class="row">
                <div class="col-lg-4 offset-lg-4 col-xl-4 offset-xl-4 text-center">
                    <img class="img-fluid" src="{{ asset('front/img/illustrations/i5.png') }}" srcset="{{ asset('front/assets/img/illustrations/i5@2x.png') }} 2x" alt="" />
                    <br />
                    <br />
                    <a href="{{ route('accountProfil') }}" class="btn btn-primary rounded-pill"><i class="uil uil-check"></i> Selesai & Lihat Profil Saya</a>
                </div>
            </div>
        </div>
    </section>
@endsection