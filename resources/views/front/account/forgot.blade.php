<div class="modal fade" id="modal-forgot" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content text-center">
            <div class="modal-body">
                <button class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <h3 class="mb-4">Anda Lupa Password ?</h3>
                <form class="text-start mb-3 form-send"
                      action="{{ route('resetPasswordSendEmail') }}"
                      method="post"
                      data-alert-show-success-status="true"
                      data-alert-show-success-title="Terkirim"
                      data-alert-show-success-message="Silahkan check email Anda, untuk dapat melakukan Reset Password">
                    {{ csrf_field() }}
                    <div class="form-floating mb-4">
                        <input type="text" class="form-control" placeholder="Email" id="loginEmail" name="email">
                        <label for="loginEmail">Email</label>
                    </div>
                    <button type="submit" class="btn btn-primary rounded-pill btn-login w-100 mb-2"> <i class="uil uil-navigator"></i> Kirim Konfirmasi Email</button>
                </form>
                <p class="mb-0">Apa Anda belum punya Akun? <a href="#" class="hover" data-bs-toggle="modal" data-bs-target="#modal-daftar">Daftar Disini</a></p>
            </div>
        </div>
    </div>
</div>