@extends('front.front.index')

@section('css')

@endsection

@section('js')

@endsection

@section('content')

    <section class="wrapper bg-light angled upper-end mt-10">
        <div class="container pb-11">
            <div class="row">
                <div class="col-lg-5 col-xl-5">
                    <img class="img-fluid" src="{{ asset('front/img/reset-password.svg') }}"/>
                </div>
                <div class="col-lg-6 offset-lg-1 col-xl-6 offset-xl-1">
                    <form class="contact-form needs-validation form-send"
                          method="post"
                          action="{{ route('resetPasswordProcess', ['id_guru' => Main::encrypt($guru->id_guru)]) }}"
                          data-alert-show-success-status="true"
                          data-alert-show-success-title="Berhasil"
                          data-alert-show-success-message="Anda dapat Login dengan Password Terbaru"
                          data-redirect="{{ route('frontBeranda') }}"
                          >
                        {{ csrf_field() }}

                        <div class="row gx-4">
                            <div class="col-md-12">
                                <div class="form-floating mb-4 text-center">
                                    <h1>Reset Password</h1>
                                    <p>Silahkan isi field dibawah untuk Reset Password.</p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="password" name="password" class="form-control"
                                           placeholder="Doe">
                                    <label for="form_lastname">Password Baru *</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating mb-4">
                                    <input id="form_lastname" type="password" name="password_conf" class="form-control"
                                           placeholder="Doe">
                                    <label for="form_lastname">Ulangi Password Baru *</label>
                                </div>
                            </div>

                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary rounded-pill"><i class="uil uil-check"></i>
                                    Simpan & Langkah Selanjutnya
                                </button>
                                <p class="text-muted"><strong>*</strong> Kolom perlu diisi.</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection