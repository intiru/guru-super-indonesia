@extends('front/front/index')

@section('content')
    <section class="wrapper bg-light">
        <div class="container py-14 py-md-12">
            <h2 class="display-4 mb-3 text-center">Form Kelengkapan Data Guru</h2>
            <p class="lead fs-lg mb-14 text-center">Data Guru yang akan digunakan untuk informasi penampilan data pada website.</p>
            <div class="row gx-lg-8 gx-xl-12 gy-6 process-wrapper line">
                <div class="col-md-6 col-lg-4">
                    <span class="icon btn btn-circle btn-lg {{ $step == 1 ? 'btn-primary' :'btn-soft-primary' }}  disabled mb-4">
                        <span class="number">01</span>
                    </span>
                    <h4 class="mb-1">Data Login User</h4>
                    <p class="mb-0">
                        Data yang digunakan sebagai dasar login, yaitu Nama, Email & Password.
                    </p>
                </div>
                <div class="col-md-6 col-lg-4">
                    <span class="icon btn btn-circle btn-lg  {{ $step == 2 ? 'btn-primary' :'btn-soft-primary' }} disabled mb-4">
                        <span class="number">02</span>
                    </span>
                    <h4 class="mb-1">Data Guru</h4>
                    <p class="mb-0">
                        Data Guru yang diperlukan untuk proses Informasi Sistem.
                    </p>
                </div>
                <div class="col-md-6 col-lg-4">
                    <span class="icon btn btn-circle btn-lg  {{ $step == 3 ? 'btn-primary' :'btn-soft-primary' }} disabled mb-4">
                        <span class="number"><i class="uil uil-check"></i></span>
                    </span>
                    <h4 class="mb-1">Selesai</h4>
                    <p class="mb-0">Anda sudah Selesai melengkapi Data Guru Super Indonesia.</p>
                </div>
            </div>
        </div>
    </section>

    @yield('content_sub')

@endsection