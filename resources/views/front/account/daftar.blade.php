<div class="modal fade" id="modal-daftar" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content text-center">
            <div class="modal-body">
                <button class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <h3 class="mb-4">Daftar Guru Super Indonesia</h3>
                <form class="text-start mb-3 form-send was-validated"
                      action="{{ route('registerProcess') }}"
                      method="post"
                      novalidate
                      data-alert-show-success-status="success"
                      data-alert-show-success-title="Pendaftaran Berhasil"
                      data-alert-show-success-message="Silahkan check email Anda untuk Verifikasi Pendaftaran">
                    {{ csrf_field() }}

                    <div class="form-floating mb-4">
                        <input type="text" class="form-control" name="nama_depan" placeholder="Nama Depan">
                        <label>Nama Depan</label>
                    </div>
                    <div class="form-floating mb-4">
                        <input type="text" class="form-control" name="nama_belakang" placeholder="Nama Belakang">
                        <label>Nama Belakang</label>
                    </div>
                    <div class="form-floating mb-4">
                        <input type="text" class="form-control" name="email" placeholder="Email">
                        <label>Email</label>
                    </div>
                    <div class="form-floating mb-4">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                        <label>Password</label>
                    </div>
                    <button type="submit" class="btn btn-primary rounded-pill btn-login w-100 mb-2"> <i class="uil uil-navigator"></i> Kirim Pendaftaran</button>
                </form>
                <p class="mb-0">Apa Anda sudah punya Akun ? <a href="#" class="hover" data-bs-toggle="modal" data-bs-target="#modal-login">Login Disini</a></p>
                <div class="divider-icon my-4">atau</div>
                <nav class="nav justify-content-center text-center">
                    <a href="{{ route('redirectGoogle') }}" class="btn btn-red btn-sm rounded-pill" style="width: 100%; display: block; margin-bottom: 4px"><i class="uil uil-google font-white"></i> Daftar via Google</a>
                    <a href="{{ route('redirectFacebook') }}" class="btn btn-blue btn-sm rounded-pill " style="width: 100%; display: block"><i class="uil uil-facebook font-white"></i> Daftar via Facebook</a>
                </nav>
            </div>
        </div>
    </div>
</div>