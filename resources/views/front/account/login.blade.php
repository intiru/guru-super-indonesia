<div class="modal fade" id="modal-login" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content text-center">
            <div class="modal-body">
                <button class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <h3 class="mb-4">Login Guru Super Indonesia</h3>
                <form action="{{ route('loginProcess') }}"
                      method="post"
                      class="text-start mb-3 form-send">
                    {{ csrf_field() }}
                    <div class="form-floating mb-4">
                        <input type="text" class="form-control" name="email" placeholder="Email" id="loginEmail">
                        <label for="loginEmail">Email</label>
                    </div>
                    <div class="form-floating mb-4">
                        <input type="password" class="form-control" name="password" placeholder="Password"
                               id="loginPassword">
                        <label for="loginPassword">Password</label>
                    </div>
                    <button type="submit" class="btn btn-primary rounded-pill btn-login w-100 mb-2"><i
                                class="uil uil-navigator"></i> Log In
                    </button>
                </form>
                <p class="mb-1"><a href="#" class="hover" data-bs-toggle="modal" data-bs-target="#modal-forgot">Lupa
                        Password ? Klik Disini</a></p>
                <p class="mb-0">Apa Anda belum punya Akun? <a href="#" class="hover" data-bs-toggle="modal"
                                                              data-bs-target="#modal-daftar">Daftar Disini</a></p>
                <div class="divider-icon my-4">atau</div>
                <nav class="nav justify-content-center text-center">
                    <a href="{{ route('redirectGoogle') }}" class="btn btn-red btn-sm rounded-pill"
                       style="width: 100%; display: block; margin-bottom: 4px"><i class="uil uil-google font-white"></i>
                        Login via Google</a>
                    <a href="{{ route('redirectFacebook') }}" class="btn btn-blue btn-sm rounded-pill " style="width: 100%; display: block"><i
                                class="uil uil-facebook font-white"></i> Login via Facebook</a>
                </nav>
            </div>
        </div>
    </div>
</div>