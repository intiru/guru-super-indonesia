@extends('front.front.index')

@section('content')
    <section class="wrapper image-wrapper bg-image bg-overlay text-white"
             data-image-src="{{ asset('upload/'.$page->blg_gambar) }}">
        <div class="container pt-17 pb-13 pt-md-19 pb-md-17 text-center">
            <div class="row">
                <div class="col-md-10 col-xl-8 mx-auto">
                    <div class="post-header">
                        <div class="post-category text-line text-white">
                            <a href="#" class="text-reset" rel="category">{{ $page->blc_judul }}</a>
                        </div>
                        <h1 class="display-1 mb-4 text-white">{{ $page->blg_judul }}</h1>
                        <ul class="post-meta text-white">
                            <li class="post-date"><i
                                        class="uil uil-calendar-alt"></i><span>{{ Main::format_date_label($page->created_at) }}</span>
                            </li>
                            <li class="post-author"><i class="uil uil-user"></i><a href="#" class="text-reset"><span>Admin Guru Indonesia</span></a>
                            </li>
                            <li class="post-likes"><i class="uil uil-eye"></i><a href="#"
                                                                                 class="text-reset">{{ Main::format_number($page->blg_views) }}
                                    <span> Views</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wrapper bg-light">
        <div class="container py-14 py-md-16">
            <div class="row gx-lg-8 gx-xl-12">
                <div class="col-lg-8">
                    <div class="blog single">
                        <div class="card">
                            <figure class="card-img-top"><img src="{{ asset('upload/'.$page->blg_gambar) }}"
                                                              alt="{{ $page->blg_judul }}"/></figure>
                            <div class="card-body">
                                <div class="classic-view">
                                    <article class="post">
                                        <div class="post-content mb-5">
                                            <h2 class="h1 mb-4">{{ $page->blg_judul }}</h2>
                                            {!! $page->blg_isi !!}
                                        </div>
                                        <div class="post-footer d-md-flex flex-md-row justify-content-md-between align-items-center mt-8">
                                            <div>
                                                <ul class="list-unstyled tag-list mb-0">
                                                    <li><a href="#"
                                                           class="btn btn-soft-ash btn-sm rounded-pill mb-0">{{ $page->blc_judul }}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="mb-0 mb-md-2">
                                                <div class="dropdown share-dropdown btn-group">
                                                    <button class="btn btn-sm btn-red rounded-pill btn-icon btn-icon-start dropdown-toggle mb-0 me-0"
                                                            data-bs-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <i class="uil uil-share-alt"></i> Share
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="#"><i
                                                                    class="uil uil-twitter"></i>Twitter</a>
                                                        <a class="dropdown-item" href="#"><i
                                                                    class="uil uil-facebook-f"></i>Facebook</a>
                                                        <a class="dropdown-item" href="#"><i
                                                                    class="uil uil-linkedin"></i>Linkedin</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <hr/>
                                <div class="author-info d-md-flex align-items-center mb-3">
                                    <div class="d-flex align-items-center">
                                        <figure class="user-avatar">
                                            <img class="rounded-circle" alt=""
                                                 src="{{ asset('front/img/avatars/u5.jpg') }}"/>
                                        </figure>
                                        <div>
                                            <h6><a href="#" class="link-dark">Mario Teguh</a></h6>
                                            <span class="post-meta fs-15">Guru Super Indonesia</span>
                                        </div>
                                    </div>
                                </div>
                                <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum
                                    massa justo sit amet risus. Maecenas faucibus mollis interdum. Fusce dapibus, tellus
                                    ac. Maecenas faucibus mollis interdum.</p>

                                <hr/>
                                <h3 class="mb-6">Blog lainnya</h3>
                                <div class="carousel owl-carousel blog grid-view mb-16" data-margin="30"
                                     data-dots="true" data-autoplay="false" data-autoplay-timeout="5000"
                                     data-responsive='{"0":{"items": "1"}, "768":{"items": "2"}, "992":{"items": "2"}, "1200":{"items": "2"}}'>
                                    @foreach($blog_others as $row)
                                        @php
                                            $link_blog_category = url('blog/'.str_slug($row->blc_judul));
                                            $link_blog = url('blog/'.str_slug($row->blg_judul));
                                        @endphp
                                        <div class="item">
                                            <article>
                                                <figure class="overlay overlay-1 hover-scale rounded mb-5">
                                                    <a href="{{ $link_blog }}">
                                                        <img src="{{ asset('upload/'.$row->blg_gambar) }}" alt=""/>
                                                    </a>
                                                    <figcaption>
                                                        <h5 class="from-top mb-0">Baca selanjutnya</h5>
                                                    </figcaption>
                                                </figure>
                                                <div class="post-header">
                                                    <div class="post-category text-line">
                                                        <a href="{{ $link_blog_category }}" class="hover" rel="category">{{ $row->blc_judul }}</a>
                                                    </div>
                                                    <h2 class="post-title h3 mt-1 mb-3">
                                                        <a class="link-dark" href="{{ $link_blog }}">
                                                            {{ $row->blg_judul }}
                                                        </a>
                                                    </h2>
                                                </div>
                                                <div class="post-footer">
                                                    <ul class="post-meta mb-0">
                                                        <li class="post-date">
                                                            <i class="uil uil-calendar-alt"></i><span>{{ Main::format_date_label($row->created_at) }}</span>
                                                        </li>
                                                        <li class="post-comments"><a href="#"><i
                                                                        class="uil uil-eye"></i>{{ Main::format_number($row->blg_views) }}</a></li>
                                                    </ul>
                                                </div>
                                            </article>
                                        </div>
                                    @endforeach
                                </div>
                                <hr/>
                            </div>
                        </div>
                    </div>
                </div>
                <aside class="col-lg-4 sidebar mt-11 mt-lg-6">
                    <div class="widget">
                        <h4 class="widget-title mb-3">Tentang Guru Indonesia</h4>
                        <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum. Nulla
                            vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus.</p>
                        <nav class="nav social">
                            <a href="#"><i class="uil uil-twitter"></i></a>
                            <a href="#"><i class="uil uil-facebook-f"></i></a>
                            <a href="#"><i class="uil uil-dribbble"></i></a>
                            <a href="#"><i class="uil uil-instagram"></i></a>
                            <a href="#"><i class="uil uil-youtube"></i></a>
                        </nav>
                        <div class="clearfix"></div>
                    </div>
                    <div class="widget">
                        <h4 class="widget-title mb-3">Blog Populer</h4>
                        <ul class="image-list">
                            @foreach($blog_populer as $row)
                                @php
                                    $link_blog = url('blog/'.str_slug($row->blg_judul));
                                @endphp
                            <li>
                                <figure class="rounded">
                                    <a href="{{ $link_blog }}">
                                        <img src="{{ asset('upload/'.$row->blg_gambar) }}" alt="{{ $row->blg_judul }}"/>
                                    </a>
                                </figure>
                                <div class="post-content">
                                    <h6 class="mb-2">
                                        <a class="link-dark" href="{{ $link_blog }}">
                                            {{ $row->blg_judul }}
                                        </a>
                                    </h6>
                                    <ul class="post-meta">
                                        <li class="post-date">
                                            <i  class="uil uil-calendar-alt"></i><span>{{ Main::format_date($row->created_at) }}</span>
                                        </li>
                                        <li class="post-comments"><a href="{{ $link_blog }}"><i class="uil uil-comment"></i>{{ Main::format_number($row->blg_views) }}</a></li>
                                    </ul>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="widget">
                        <h4 class="widget-title mb-3">Kategori Blog</h4>
                        <ul class="unordered-list bullet-primary text-reset">
                            @foreach($blog_category as $row)
                                <li><a href="{{ url('/blog/'.str_slug($row->blc_judul)) }}">{{ $row->blc_judul }} ({{ Main::format_number($row->blog_total) }})</a></li>
                            @endforeach
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </section>
@endsection