@extends('front/front/index')

@section('content')
    <section class="wrapper image-wrapper bg-image bg-overlay text-white" data-image-src="{{ asset('front/img/photos/bg1.jpg') }}">
        <div class="container pt-19 pt-md-21 pb-18 pb-md-20 text-center">
            <div class="row">
                <div class="col-md-10 col-lg-8 col-xl-7 col-xxl-6 mx-auto">
                    <h1 class="display-1 text-white mb-3">Our Services</h1>
                    <p class="lead fs-lg px-md-3 px-lg-7 px-xl-9 px-xxl-10">We are a creative company that focuses on
                        establishing long-term relationships with customers.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="wrapper bg-light">
        <div class="container pt-14 pb-12 pt-md-16 pb-md-14">
            <div class="row gx-lg-8 gx-xl-12 gy-10 mb-lg-22 mb-xl-24 align-items-center">
                <div class="col-lg-7 order-lg-2">
                    <div class="row gx-md-5 gy-5">
                        <div class="col-md-5 offset-md-1 align-self-end">
                            <div class="card bg-pale-yellow">
                                <div class="card-body">
                                    <img src="{{ asset('front/img/icons/lineal/telephone-3.svg') }}"
                                         class="svg-inject icon-svg icon-svg-md text-yellow mb-3" alt=""/>
                                    <h4>24/7 Support</h4>
                                    <p class="mb-0">Nulla vitae elit libero, a pharetra augue. Donec id elit non mi
                                        porta.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 align-self-end">
                            <div class="card bg-pale-red">
                                <div class="card-body">
                                    <img src="{{ asset('front/img/icons/lineal/shield.svg') }}"
                                         class="svg-inject icon-svg icon-svg-md text-red mb-3" alt=""/>
                                    <h4>Secure Payments</h4>
                                    <p class="mb-0">Nulla vitae elit libero, a pharetra augue. Donec id elit non mi
                                        porta.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="card bg-pale-leaf">
                                <div class="card-body">
                                    <img src="{{ asset('front/img/icons/lineal/cloud-computing-3.svg') }}"
                                         class="svg-inject icon-svg icon-svg-md text-leaf mb-3" alt=""/>
                                    <h4>Daily Updates</h4>
                                    <p class="mb-0">Nulla vitae elit libero, a pharetra augue.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 align-self-start">
                            <div class="card bg-pale-primary">
                                <div class="card-body">
                                    <img src="{{ asset('front/img/icons/lineal/analytics.svg') }}"
                                         class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt=""/>
                                    <h4>Market Research</h4>
                                    <p class="mb-0">Nulla vitae elit libero, a pharetra augue. Donec id elit non mi
                                        porta gravida at eget.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <h2 class="fs-15 text-uppercase text-muted mb-3">What We Do?</h2>
                    <h3 class="display-4 mb-5">The service we offer is specifically designed to meet your needs.</h3>
                    <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas sed diam eget
                        risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum. Praesent commodo
                        cursus magna, vel scelerisque nisl consectetur et.</p>
                    <a href="services2.html#" class="btn btn-navy rounded-pill mt-3">More Details</a>
                </div>
            </div>
        </div>
    </section>
    <section class="wrapper bg-soft-primary">
        <div class="container py-14 pt-md-16 pt-lg-0 pb-md-16">
            <div class="row text-center">
                <div class="col-lg-10 mx-auto">
                    <div class="mt-lg-n20 mt-xl-n22 position-relative">
                        <div class="shape bg-dot red rellax w-16 h-18" data-rellax-speed="1"
                             style="top: 1rem; left: -3.9rem;"></div>
                        <div class="shape rounded-circle bg-line primary rellax w-18 h-18" data-rellax-speed="1"
                             style="bottom: 2rem; right: -3rem;"></div>
                        <video poster="{{ asset('front/img/photos/movie.jpg') }}" class="player" playsinline controls
                               preload="none">
                            <source src="{{ asset('front/media/movie.mp4') }}" type="video/mp4">
                            <source src="{{ asset('front/media/movie.webm') }}" type="video/webm">
                        </video>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-lg-9 mx-auto">
                    <h2 class="fs-15 text-uppercase text-muted mb-3 mt-12">Our Process</h2>
                    <h3 class="display-4 mb-0 text-center px-xl-10 px-xxl-15">Find out everything you need to know about
                        creating a business process model</h3>
                    <div class="row gx-lg-8 gx-xl-12 process-wrapper text-center mt-9">
                        <div class="col-md-4"><img src="{{ asset('front/img/icons/lineal/light-bulb.svg') }}"
                                                   class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt=""/>
                            <h4 class="mb-1">1. Concept</h4>
                            <p>Nulla vitae elit libero elit non porta gravida eget metus cras.</p>
                        </div>
                        <div class="col-md-4"><img src="{{ asset('front/img/icons/lineal/settings-3.svg') }}"
                                                   class="svg-inject icon-svg icon-svg-md text-red mb-3" alt=""/>
                            <h4 class="mb-1">2. Prepare</h4>
                            <p>Nulla vitae elit libero elit non porta gravida eget metus cras.</p>
                        </div>
                        <div class="col-md-4"><img src="{{ asset('front/img/icons/lineal/design.svg') }}"
                                                   class="svg-inject icon-svg icon-svg-md text-leaf mb-3" alt=""/>
                            <h4 class="mb-1">3. Retouch</h4>
                            <p>Nulla vitae elit libero elit non porta gravida eget metus cras.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wrapper bg-light">
        <div class="container py-14 py-md-16">
            <div class="row gx-lg-8 gx-xl-12 gy-10 mb-14 mb-md-16 align-items-center">
                <div class="col-lg-7">
                    <figure><img class="w-auto" src="{{ asset('front/img/illustrations/i8.png') }}"
                                 srcset="{{ asset('front/img/illustrations/i8@2x.png') }} 2x" alt=""/></figure>
                </div>
                <div class="col-lg-5">
                    <h3 class="display-4 mb-6 pe-xxl-6">We bring rapid solutions to make the life of our customers
                        easier.</h3>
                    <ul class="progress-list mt-3">
                        <li>
                            <p>Marketing</p>
                            <div class="progressbar line blue" data-value="100"></div>
                        </li>
                        <li>
                            <p>Strategy</p>
                            <div class="progressbar line yellow" data-value="80"></div>
                        </li>
                        <li>
                            <p>Development</p>
                            <div class="progressbar line orange" data-value="85"></div>
                        </li>
                        <li>
                            <p>Data Analysis</p>
                            <div class="progressbar line green" data-value="90"></div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                <div class="col-lg-7 order-lg-2">
                    <figure><img class="w-auto" src="{{ asset('front/img/illustrations/i7.png') }}"
                                 srcset="{{ asset('front/img/illustrations/i7@2x.png') }} 2x" alt=""/></figure>
                </div>
                <div class="col-lg-5">
                    <h3 class="display-4 mb-5">We make your spending stress-free for you to have the perfect
                        control.</h3>
                    <p class="mb-6">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                        mus. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Praesent commodo cursus magna,
                        vel scelerisque nisl consectetur et.</p>
                    <div class="row gy-3">
                        <div class="col-xl-6">
                            <ul class="icon-list bullet-bg bullet-soft-primary mb-0">
                                <li><span><i class="uil uil-check"></i></span><span>Aenean quam ornare. Curabitur blandit.</span>
                                </li>
                                <li class="mt-3"><span><i class="uil uil-check"></i></span><span>Nullam quis risus eget urna mollis ornare.</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xl-6">
                            <ul class="icon-list bullet-bg bullet-soft-primary mb-0">
                                <li><span><i class="uil uil-check"></i></span><span>Etiam porta euismod malesuada mollis.</span>
                                </li>
                                <li class="mt-3"><span><i class="uil uil-check"></i></span><span>Vivamus sagittis lacus vel augue rutrum.</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wrapper bg-soft-primary">
        <div class="container pt-14 pb-18 pt-md-16 pb-md-22 text-center">
            <div class="row">
                <div class="col-lg-10 col-xl-9 col-xxl-8 mx-auto">
                    <h2 class="fs-15 text-uppercase text-muted mb-3">Our Pricing</h2>
                    <h3 class="display-4 mb-15 mb-md-6 px-lg-10">We offer great prices, premium products and quality
                        service for your business.</h3>
                </div>
            </div>
        </div>
    </section>
    <section class="wrapper bg-light">
        <div class="container py-14 py-md-16">
            <div class="pricing-wrapper position-relative mt-n22 mt-md-n24">
                <div class="shape bg-dot primary rellax w-16 h-18" data-rellax-speed="1"
                     style="top: 2rem; right: -2.4rem;"></div>
                <div class="shape rounded-circle bg-line red rellax w-18 h-18 d-none d-lg-block" data-rellax-speed="1"
                     style="bottom: 0.5rem; left: -2.5rem;"></div>
                <div class="pricing-switcher-wrapper switcher">
                    <p class="mb-0 pe-3">Monthly</p>
                    <div class="pricing-switchers">
                        <div class="pricing-switcher pricing-switcher-active"></div>
                        <div class="pricing-switcher"></div>
                        <div class="switcher-button bg-primary"></div>
                    </div>
                    <p class="mb-0 ps-3">Yearly</p>
                </div>
                <div class="row gy-6 mt-3 mt-md-5">
                    <div class="col-md-6 col-lg-4">
                        <div class="pricing card text-center">
                            <div class="card-body">
                                <img src="{{ asset('front/img/icons/lineal/shopping-basket.svg') }}"
                                     class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt=""/>
                                <h4 class="card-title">Basic Plan</h4>
                                <div class="prices text-dark">
                                    <div class="price price-show"><span class="price-currency">$</span><span
                                                class="price-value">9</span> <span class="price-duration">month</span>
                                    </div>
                                    <div class="price price-hide price-hidden"><span
                                                class="price-currency">$</span><span class="price-value">99</span> <span
                                                class="price-duration">year</span></div>
                                </div>
                                <ul class="icon-list bullet-bg bullet-soft-primary mt-8 mb-9 text-start">
                                    <li><i class="uil uil-check"></i><span><strong>1</strong> Project </span></li>
                                    <li><i class="uil uil-check"></i><span><strong>100K</strong> API Access </span></li>
                                    <li><i class="uil uil-check"></i><span><strong>100MB</strong> Storage </span></li>
                                    <li>
                                        <i class="uil uil-times bullet-soft-red"></i><span> Weekly <strong>Reports</strong> </span>
                                    </li>
                                    <li>
                                        <i class="uil uil-times bullet-soft-red"></i><span> 7/24 <strong>Support</strong></span>
                                    </li>
                                </ul>
                                <a href="services2.html#" class="btn btn-primary rounded-pill">Choose Plan</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 popular">
                        <div class="pricing card text-center">
                            <div class="card-body">
                                <img src="{{ asset('front/img/icons/lineal/home.svg') }}"
                                     class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt=""/>
                                <h4 class="card-title">Premium Plan</h4>
                                <div class="prices text-dark">
                                    <div class="price price-show"><span class="price-currency">$</span><span
                                                class="price-value">19</span> <span class="price-duration">month</span>
                                    </div>
                                    <div class="price price-hide price-hidden"><span
                                                class="price-currency">$</span><span class="price-value">199</span>
                                        <span class="price-duration">year</span></div>
                                </div>
                                <ul class="icon-list bullet-bg bullet-soft-primary mt-8 mb-9 text-start">
                                    <li><i class="uil uil-check"></i><span><strong>5</strong> Projects </span></li>
                                    <li><i class="uil uil-check"></i><span><strong>100K</strong> API Access </span></li>
                                    <li><i class="uil uil-check"></i><span><strong>200MB</strong> Storage </span></li>
                                    <li><i class="uil uil-check"></i><span> Weekly <strong>Reports</strong></span></li>
                                    <li>
                                        <i class="uil uil-times bullet-soft-red"></i><span> 7/24 <strong>Support</strong></span>
                                    </li>
                                </ul>
                                <a href="services2.html#" class="btn btn-primary rounded-pill">Choose Plan</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 offset-md-3 col-lg-4 offset-lg-0">
                        <div class="pricing card text-center">
                            <div class="card-body">
                                <img src="{{ asset('front/img/icons/lineal/briefcase-2.svg') }}"
                                     class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt=""/>
                                <h4 class="card-title">Corporate Plan</h4>
                                <div class="prices text-dark">
                                    <div class="price price-show"><span class="price-currency">$</span><span
                                                class="price-value">49</span> <span class="price-duration">month</span>
                                    </div>
                                    <div class="price price-hide price-hidden"><span
                                                class="price-currency">$</span><span class="price-value">499</span>
                                        <span class="price-duration">year</span></div>
                                </div>
                                <ul class="icon-list bullet-bg bullet-soft-primary mt-8 mb-9 text-start">
                                    <li><i class="uil uil-check"></i><span><strong>20</strong> Projects </span></li>
                                    <li><i class="uil uil-check"></i><span><strong>300K</strong> API Access </span></li>
                                    <li><i class="uil uil-check"></i><span><strong>500MB</strong> Storage </span></li>
                                    <li><i class="uil uil-check"></i><span> Weekly <strong>Reports</strong></span></li>
                                    <li><i class="uil uil-check"></i><span> 7/24 <strong>Support</strong></span></li>
                                </ul>
                                <a href="services2.html#" class="btn btn-primary rounded-pill">Choose Plan</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wrapper bg-light angled upper-end lower-end wrapper image-wrapper bg-auto no-overlay bg-image text-center bg-map"
             data-image-src="{{ asset('front/img/map.png') }}">
        <div class="container pt-0 pb-14 pt-md-16 pb-md-18">
            <div class="row">
                <div class="col-lg-10 col-xl-9 col-xxl-8 mx-auto">
                    <h2 class="fs-15 text-uppercase text-muted mb-3">Join Our Community</h2>
                    <h3 class="display-4 mb-8 px-lg-12">We are trusted by over 5000+ clients. Join them now and grow
                        your business.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-lg-9 col-xl-7 mx-auto">
                    <div class="row align-items-center counter-wrapper gy-4 gy-md-0">
                        <div class="col-md-4 text-center">
                            <h3 class="counter counter-lg text-primary">7518</h3>
                            <p>Completed Projects</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <h3 class="counter counter-lg text-primary">5472</h3>
                            <p>Satisfied Customers</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <h3 class="counter counter-lg text-primary">2184</h3>
                            <p>Expert Employees</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection