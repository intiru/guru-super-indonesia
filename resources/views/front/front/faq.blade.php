@extends('front/front/index')

@section('content')
    <section class="wrapper image-wrapper bg-image bg-overlay text-white"
             data-image-src="{{ asset('front/img/photos/bg1.jpg') }}">
        <div class="container pt-19 pt-md-21 pb-18 pb-md-20 text-center">
            <div class="row">
                <div class="col-md-10 col-lg-8 col-xl-7 col-xxl-6 mx-auto">
                    <h1 class="display-1 text-white mb-3">Frequently Ask Question</h1>
                    <p class="lead fs-lg px-md-3 px-lg-7 px-xl-9 px-xxl-10">We are a creative company that focuses on
                        establishing long-term relationships with customers.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="wrapper bg-light">
        <div class="container pt-8 pt-md-14">
            <div class="row gx-lg-8 gx-xl-121 gy-10 mb-lg-12 mb-xl-12 align-items-center">
                <div class="accordion accordion-wrapper mt-10" id="accordion">
                    <div class="card accordion-item">
                        <div class="card-header" id="faq-1">
                            <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#faq-collapse-1" aria-expanded="true" aria-controls="faq-collapse-1"> Can I use this template without using Gulp or SCSS? </button>
                        </div>
                        <!--/.card-header -->
                        <div id="faq-collapse-1" class="accordion-collapse collapse" aria-labelledby="faq-1">
                            <div class="card-body">
                                <p>Yes. Gulp is optional. You can use plain HTML / CSS / JS to customize Sandbox. Files you need are located in <code class="folder">dev</code> folder.</p>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.accordion-collapse -->
                    </div>
                    <!--/.accordion-item -->
                    <div class="card accordion-item">
                        <div class="card-header" id="faq-2">
                            <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#faq-collapse-2" aria-expanded="true" aria-controls="faq-collapse-2"> How can I remove unwanted plugins?</button>
                        </div>
                        <!--/.card-header -->
                        <div id="faq-collapse-2" class="accordion-collapse collapse" aria-labelledby="faq-2">
                            <div class="card-body">
                                <p>All third-party plugin JS files are located in <code class="folder">src/assets/js/vendor</code> and their CSS files are in <code class="folder">src/assets/css/vendor</code>. Just remove unwanted vendor JS / CSS files from vendor folders and then remove unwanted functions from <code class="file">src/assets/js/theme.js</code> and recompile.</p>
                                <p>If you're <strong>not using</strong> Gulp, you can remove unwanted plugins manually from <code class="file">dev/assets/js/plugins.js</code>, <code class="file">dev/assets/js/theme.js</code> and their CSS from <code class="file">dev/assets/css/plugins.css</code>.</p>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.accordion-collapse -->
                    </div>
                    <!--/.accordion-item -->
                    <div class="card accordion-item">
                        <div class="card-header" id="faq-6">
                            <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#faq-collapse-6" aria-expanded="true" aria-controls="faq-collapse-6">How can I remove unwanted CSS?</button>
                        </div>
                        <!--/.card-header -->
                        <div id="faq-collapse-6" class="accordion-collapse collapse" aria-labelledby="faq-6">
                            <div class="card-body">
                                <p>Bootstrap SCSS imports are located in <code class="file">src/assets/scss/_bootstrap.scss</code> and theme SCSS imports are in <code class="file">src/assets/scss/theme/_theme.scss</code>. Remove or comment any unwanted import and recompile.</p>
                                <p>If you're <strong>not using</strong> Gulp, you can remove unwanted CSS manually from <code class="file">dev/assets/css/style.css</code></p>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.accordion-collapse -->
                    </div>
                    <!--/.accordion-item -->
                    <div class="card accordion-item">
                        <div class="card-header" id="faq-3">
                            <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#faq-collapse-3" aria-expanded="true" aria-controls="faq-collapse-3"> Does Sandbox support RTL? </button>
                        </div>
                        <!--/.card-header -->
                        <div id="faq-collapse-3" class="accordion-collapse collapse" aria-labelledby="faq-3">
                            <div class="card-body">
                                <p>No, not currently. Although with the use of <a href="https://rtlcss.com/learn/usage-guide/install/" target="_blank" class="external">RTLCSS</a> project you can generate RTL version of <code class="file">style.css</code>, however some template specific styles won’t have support for RTL out of the box.</p>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.accordion-collapse -->
                    </div>
                    <!--/.accordion-item -->
                    <div class="card accordion-item">
                        <div class="card-header" id="faq-4">
                            <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#faq-collapse-4" aria-expanded="true" aria-controls="faq-collapse-4"> Why SVG icons appear black? </button>
                        </div>
                        <!--/.card-header -->
                        <div id="faq-collapse-4" class="accordion-collapse collapse" aria-labelledby="faq-4">
                            <div class="card-body">
                                <p>Due to the <a href="https://en.wikipedia.org/wiki/Same-origin_policy" target="_blank" class="external">same-origin policy</a> SVGInject does not work when run from the local file system in many browsers (Chrome, Safari). Please test on a working web server.</p>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.accordion-collapse -->
                    </div>
                    <!--/.accordion-item -->
                    <div class="card accordion-item">
                        <div class="card-header" id="faq-5">
                            <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#faq-collapse-5" aria-expanded="true" aria-controls="faq-collapse-5"> How to make the contact or newsletter forms work? </button>
                        </div>
                        <!--/.card-header -->
                        <div id="faq-collapse-5" class="accordion-collapse collapse" aria-labelledby="faq-5">
                            <div class="card-body">
                                <p>Follow the instructions <a href="forms.html" target="_blank" class="external">here</a> to reach the guide on configuring the contact or newsletter forms in Sandbox. If the forms don't work or if you receive any errors please keep in mind that the contact forms won't work on local environment. Please test them on a working web server.</p>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.accordion-collapse -->
                    </div>
                    <!--/.accordion-item -->
                    <div class="card accordion-item">
                        <div class="card-header" id="faq-7">
                            <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#faq-collapse-7" aria-expanded="true" aria-controls="faq-collapse-7"> Does Sandbox require jQuery? </button>
                        </div>
                        <!--/.card-header -->
                        <div id="faq-collapse-7" class="accordion-collapse collapse" aria-labelledby="faq-7">
                            <div class="card-body">
                                <p>Yes, some plugins that are included in the template are jQuery dependent.</p>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.accordion-collapse -->
                    </div>
                    <!--/.accordion-item -->
                    <div class="card accordion-item">
                        <div class="card-header" id="faq-8">
                            <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#faq-collapse-8" aria-expanded="true" aria-controls="faq-collapse-8"> Why am I getting an error while installing to Wordpress? </button>
                        </div>
                        <!--/.card-header -->
                        <div id="faq-collapse-8" class="accordion-collapse collapse" aria-labelledby="faq-8">
                            <div class="card-body">
                                <p>Sandbox is an HTML template, not a Wordpress theme. So it cannot be installed in Wordpress.</p>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.accordion-collapse -->
                    </div>
                    <!--/.accordion-item -->
                    <div class="card accordion-item">
                        <div class="card-header" id="faq-9">
                            <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#faq-collapse-9" aria-expanded="true" aria-controls="faq-collapse-9"> Why the image mask doesn't work on my copy of the item? </button>
                        </div>
                        <!--/.card-header -->
                        <div id="faq-collapse-9" class="accordion-collapse collapse" aria-labelledby="faq-9">
                            <div class="card-body">
                                <p>There is a known browsers-spesific issue regarding image-mask that prevents them from working on local environments. (Running from a filesystem is now restricted, so you can no longer reference one file from another.) If you upload the template on a working server image masks should work properly.</p>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.accordion-collapse -->
                    </div>
                    <!--/.accordion-item -->
                    <div class="card accordion-item">
                        <div class="card-header" id="faq-10">
                            <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#faq-collapse-10" aria-expanded="true" aria-controls="faq-collapse-10"> How to enable sourcemaps? </button>
                        </div>
                        <!--/.card-header -->
                        <div id="faq-collapse-10" class="accordion-collapse collapse" aria-labelledby="faq-10">
                            <div class="card-body">
                                <p>You can enable sourcemaps in <code class="file">dist/asssets/css/style.css</code> by uncommenting lines <mark class="doc">141</mark> and <mark class="doc">147</mark> on <code class="file">gulpfile.js</code> and then running <kbd class="terminal bg-pale-primary">gulp serve</kbd> command.</p>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.accordion-collapse -->
                    </div>
                    <!--/.accordion-item -->
                </div>
            </div>
        </div>
    </section>

@endsection