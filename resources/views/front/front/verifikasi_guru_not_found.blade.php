@extends('front/front/index')

@section('content')
    <section class="wrapper bg-light">
        <div class="container pt-8 pt-md-14">
            <div class="row gx-lg-8 gx-xl-121 gy-10 mb-lg-12 mb-xl-12 align-items-center">
                <div class="card">
                    <div class="card-body">
                        <div class="row g-6">
                            <div class="col-sm-12 col-lg-6 offset-lg-3 text-center">
                                <img src="{{ asset('front/img/failed.jpg') }}" class="img-responsive img-fluid">
                                <h1>Data Guru Tidak Ditemukan</h1>
                                <p>Silahkan melakukan registrasi kembali atau reset password</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection