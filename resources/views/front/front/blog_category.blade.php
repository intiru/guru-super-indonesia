@extends('front.front.index')

@section('content')

    <section class="wrapper bg-soft-primary ">
        <div class="container pt-10 pb-12 pt-md-14 pb-md-16 text-center">
            <div class="row">
                <div class="col-md-7 col-lg-6 col-xl-5 mx-auto">
                    <h1 class="display-1 mb-3">Blog / {{ $page->blc_judul }}</h1>
                    <p class="lead px-lg-5 px-xxl-8">{{ $page->blc_isi }}</p>
                </div>
            </div>
        </div>
    </section>
    <section class="wrapper bg-light">
        <div class="container py-14 py-md-16">
            <div class="row gx-lg-8 gx-xl-12">
                <div class="col-lg-8">
                    <div class="blog grid grid-view">
                        <div class="row isotope gx-md-8 gy-8 mb-8">
                            @foreach($blog_list as $row)
                                @php
                                    $link_blog_category = url('blog/'.str_slug($row->blc_judul));
                                    $link_blog = url('blog/'.str_slug($row->blg_judul));
                                @endphp
                            <article class="item post col-md-6">
                                <div class="card">
                                    <figure class="card-img-top overlay overlay-1 hover-scale">
                                        <a href="{{ $link_blog }}">
                                            <img src="{{ asset('upload/'.$row->blg_gambar) }}" alt="{{ $row->blg_judul }}"/>
                                        </a>
                                        <figcaption>
                                            <h5 class="from-top mb-0">Baca Selanjutnya</h5>
                                        </figcaption>
                                    </figure>
                                    <div class="card-body">
                                        <div class="post-header">
                                            <div class="post-category text-line">
                                                <a href="{{ $link_blog_category }}" class="hover" rel="category">{{ $row->blc_judul }}</a>
                                            </div>
                                            <h2 class="post-title h3 mt-1 mb-3">
                                                <a class="link-dark"  href="{{ $link_blog }}">
                                                    {{ $row->blg_judul }}
                                                </a>
                                            </h2>
                                        </div>
                                        <div class="post-content">
                                            <p>{{ \app\Helpers\Main::short_desc($row->blg_isi) }}</p>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <ul class="post-meta d-flex mb-0">
                                            <li class="post-date">
                                                <i class="uil uil-calendar-alt"></i><span>{{ Main::format_date_label($row->created_at) }}</span>
                                            </li>
                                            <li class="post-likes ms-auto">
                                                <a href="{{ $link_blog }}">
                                                    <i class="uil uil-eye"></i>{{ Main::format_number($row->blg_views) }}
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                            @endforeach
                        </div>
                    </div>
                    <nav class="d-flex" aria-label="pagination">
                        {{ $blog_list->links() }}
                    </nav>
                </div>
                <aside class="col-lg-4 sidebar mt-8 mt-lg-6">
                    <div class="widget">
                        <h4 class="widget-title mb-3">About Us</h4>
                        <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum. Nulla
                            vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus.</p>
                        <nav class="nav social">
                            <a href="#"><i class="uil uil-twitter"></i></a>
                            <a href="#"><i class="uil uil-facebook-f"></i></a>
                            <a href="#"><i class="uil uil-dribbble"></i></a>
                            <a href="#"><i class="uil uil-instagram"></i></a>
                            <a href="#"><i class="uil uil-youtube"></i></a>
                        </nav>
                        <div class="clearfix"></div>
                    </div>
                    <div class="widget">
                        <h4 class="widget-title mb-3">Blog Populer</h4>
                        <ul class="image-list">
                            @foreach($blog_populer as $row)
                                @php
                                    $link_blog = url('blog/'.str_slug($row->blg_judul));
                                @endphp
                                <li>
                                    <figure class="rounded">
                                        <a href="{{ $link_blog }}">
                                            <img src="{{ asset('upload/'.$row->blg_gambar) }}" alt="{{ $row->blg_judul }}"/>
                                        </a>
                                    </figure>
                                    <div class="post-content">
                                        <h6 class="mb-2">
                                            <a class="link-dark" href="{{ $link_blog }}">
                                                {{ $row->blg_judul }}
                                            </a>
                                        </h6>
                                        <ul class="post-meta">
                                            <li class="post-date">
                                                <i  class="uil uil-calendar-alt"></i><span>{{ Main::format_date($row->created_at) }}</span>
                                            </li>
                                            <li class="post-comments"><a href="{{ $link_blog }}"><i class="uil uil-comment"></i>{{ Main::format_number($row->blg_views) }}</a></li>
                                        </ul>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="widget">
                        <h4 class="widget-title mb-3">Kategori Blog</h4>
                        <ul class="unordered-list bullet-primary text-reset">
                            @foreach($blog_category as $row)
                                <li><a href="{{ url('/blog/'.str_slug($row->blc_judul)) }}">{{ $row->blc_judul }} ({{ Main::format_number($row->blog_total) }})</a></li>
                            @endforeach
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </section>

@endsection