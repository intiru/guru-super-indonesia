@extends('front/front/index')

@section('content')
    <section class="wrapper bg-light">
        <div class="container pt-8 pt-md-14">
            <div class="row gx-lg-8 gx-xl-121 gy-10 mb-lg-12 mb-xl-12 align-items-center">
                <div class="card">
                    <div class="card-body">
                        <div class="row g-6">
                            <div class="col-sm-12 col-lg-4 offset-lg-4 text-center">
                                <img src="{{ asset('front/img/illustrations/i5.png') }}" class="img-responsive img-fluid">
                                <br />
                                <br />
                                <h1>Verifikasi Berhasil</h1>
                                <p>Silahkan Klik Tombol Login kembali menggunakan email dan password yang dimasukkan sebelumnya.</p>

                                <button type="button" class="btn btn-outline-gradient gradient-7 rounded-pill"
                                        data-bs-toggle="modal"
                                        data-bs-target="#modal-login">
                                    <i class="uil uil-check-circle"></i> Login Disini
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection