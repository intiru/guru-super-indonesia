<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Guru Super Indonesia">
    <meta name="keywords" content="Guru Super Indonesia">
    <meta name="author" content="elemis">
    <title>Guru Super Indonesia</title>
    <link rel="shortcut icon" href="{{ asset('front/img/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('front/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
    <link rel="preload" href="{{ asset('front/css/fonts/dm.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/loading.css') }}">
    @yield('css')
</head>

<body
        data-route-feed-comment-delete="{{ route('forumGuruCommentDelete') }}"
        data-route-feed-edit-modal="{{ route('forumGuruFeedEditModal') }}"
        data-route-feed-delete="{{ route('forumGuruFeedDelete') }}"
        data-route-feed-like="{{ route('forumGuruFeedLike') }}"
        data-csrf-token="{{ csrf_token() }}">
<div class="content-wrapper">
    <header class="wrapper bg-light">
        <div class="bg-primary text-white fw-bold fs-15 mb-2">
            <div class="container py-2 d-md-flex flex-md-row">
                <div class="d-flex flex-row align-items-center">
                    <div class="icon text-white fs-22 mt-1 me-2"><i class="uil uil-location-pin-alt"></i></div>
                    <address class="mb-0">
                        <a href="" class="link-white hover">Perkantoran Jakarta, Indonesia</a>
                    </address>
                </div>
                <div class="d-flex flex-row align-items-center me-6 ms-auto">
                    <div class="icon text-white fs-22 mt-1 me-2"><i class="uil uil-whatsapp"></i></div>
                    <p class="mb-0">
                        <a href="" target="_blank" class="link-white hover">0812-3737-6068</a>
                    </p>
                </div>
                <div class="d-flex flex-row align-items-center">
                    <div class="icon text-white fs-22 mt-1 me-2"><i class="uil uil-message"></i></div>
                    <p class="mb-0"><a href="" class="link-white hover" target="_blank">guru.indonesia@gmail.com</a></p>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg center-nav transparent navbar-light">
            <div class="container flex-lg-row flex-nowrap align-items-center">
                <div class="navbar-brand w-100">
                    <a href="{{ route('frontBeranda') }}"
                       style="font-family: 'DM Serif Display'; font-size: 2.5em; color: #3F78E0; font-weight: bold">
                        Guru Indonesia
                    </a>
                </div>
                <div class="navbar-collapse offcanvas-nav">
                    <div class="offcanvas-header d-lg-none d-xl-none">
                        <a href="{{ route('frontBeranda') }}"
                           style="font-family: 'DM Serif Display'; font-size: 2em; color: #3F78E0; font-weight: bold">
                            Guru Indonesia
                        </a>
                        <button type="button" class="btn-close btn-close-white offcanvas-close offcanvas-nav-close"
                                aria-label="Close"></button>
                    </div>
                    <ul class="navbar-nav">
                        @if(Session::get('guru_login'))
                            <li class="nav-item d-block d-sm-none">
                                <a class="nav-link" href="#"><i class="uil uil-bell"></i> Notifikasi</a>
                            </li>
                            <li class="nav-item dropdown d-block d-sm-none">
                                <a class="nav-link dropdown-toggle" href="#">
                                    <i class="uil uil-user"></i> {{ $front_guru->gru_nama_depan.' '.$front_guru->gru_nama_belakang }}
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item">
                                        <a class="dropdown-item" href="{{ route('feedSayaList') }}">
                                            <i class="uil uil-comment"></i> Feed Saya
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="dropdown-item" href="{{ route('accountProfil') }}">
                                            <i class="uil uil-user"></i> Profil Saya
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="dropdown-item" href="{{ route('logoutProcess') }}">
                                            <i class="uil uil-power"></i> Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @else
                            <li class="nav-item d-block d-sm-none">
                                <a class="nav-link" href="#"
                                   data-bs-toggle="modal"
                                   data-bs-target="#modal-daftar">
                                    <i class="uil uil-user"></i> Daftar
                                </a>
                            </li>
                            <li class="nav-item d-block d-sm-none">
                                <a class="nav-link" href="#"
                                   data-bs-toggle="modal"
                                   data-bs-target="#modal-login">
                                    <i class="uil uil-check-circle"></i>
                                    Login
                                </a>
                            </li>
                        @endif
                        <hr class="my-2 d-block d-sm-none">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('frontBeranda') }}">Beranda</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('forumList') }}">Forum Diskusi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('frontLayanan') }}">Layanan Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('frontBlogList') }}">Blog</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('frontKontak') }}">Kontak Kami</a>
                        </li>
                    </ul>
                </div>
                <div class="navbar-other w-100 d-flex ms-auto">
                    <ul class="navbar-nav flex-row align-items-center ms-auto">
                    <!--                        <li class="nav-item dropdown d-none d-sm-block">
                            <a class="btn btn-sm btn-primary rounded-pill dropdown-toggle " href="#"><i
                                        class="uil uil-user"></i> Mahendra Wardana</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item"><a class="dropdown-item"
                                                        href="{{ route('accountProfilLengkapi2') }}"><i
                                                class="uil uil-user"></i> Lengkapi Profil Saya</a></li>
                                <li class="nav-item"><a class="dropdown-item" href="{{ route('accountProfil') }}"><i
                                                class="uil uil-user"></i> Profil Saya</a></li>
                                <li class="nav-item"><a class="dropdown-item" href="{{ route('accountProfil') }}"><i
                                                class="uil uil-user"></i> Pengumuman</a></li>
                                <li class="nav-item"><a class="dropdown-item" href="#"><i class="uil uil-power"></i>
                                        Logout</a></li>
                            </ul>
                        </li>-->
                        @if(Session::get('guru_login'))
                            <li class="nav-item dropdown d-none d-sm-block">
                                <a class="nav-link dropdown-toggle" href="#">
                                    <div class="float-start">
                                        <img class="avatar w-8" src="{{ Main::guru_avatar_path() }}" alt=""/>
                                    </div>
                                    <div class="float-end" style="padding-top: 8px; padding-left: 10px">
                                        {{ $front_guru->gru_nama_depan.' '.$front_guru->gru_nama_belakang }}
                                    </div>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item">
                                        <a class="dropdown-item" href="{{ route('feedSayaList') }}">
                                            <i class="uil uil-comment"></i> Feed Saya
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="dropdown-item" href="{{ route('accountProfil') }}">
                                            <i class="uil uil-user"></i> Profil Saya
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="dropdown-item" href="{{ route('logoutProcess') }}">
                                            <i class="uil uil-power"></i> Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <img src="{{ asset('front/img/icons/solid/bell.svg') }}"
                                         class="svg-inject icon-svg icon-svg-xxs solid-mono text-warning" alt=""/>
                                </a>
                            </li>
                        @else
                            <li class="nav-item d-none d-md-block">
                                <a href="#" class="btn btn-sm btn-primary rounded-pill" data-bs-toggle="modal"
                                   data-bs-target="#modal-daftar"><i class="uil uil-user"></i> Daftar</a>
                            </li>
                            <li class="nav-item d-none d-md-block">
                                <a href="#" class="btn btn-outline-gradient gradient-7 rounded-pill"
                                   data-bs-toggle="modal"
                                   data-bs-target="#modal-login">
                                    <i class="uil uil-check-circle"></i> Login
                                </a>
                            </li>
                        @endif
                        <li class="nav-item d-lg-none">
                            <div class="navbar-hamburger">
                                <button class="hamburger animate plain" data-toggle="offcanvas-nav"><span></span>
                                </button>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    @yield('content')
</div>
<footer class="bg-dark text-inverse">
    <div class="container py-13 py-md-15">
        <div class="row gy-6 gy-lg-0">
            <div class="col-md-4 col-lg-3">
                <div class="widget" style="margin-top: -20px">
                    <a href="{{ route('frontBeranda') }}"
                       style="font-family: 'DM Serif Display'; font-size: 2em; color: #3F78E0; font-weight: bold;">
                        Guru Indonesia
                    </a>
                    <p class="mb-4">© {{ date('Y') }} LMT Team. <br class="d-none d-lg-block"/>All rights reserved.</p>
                    <nav class="nav social ">
                        <a href="#"><i class="uil uil-twitter"></i></a>
                        <a href="#"><i class="uil uil-facebook-f"></i></a>
                        <a href="#"><i class="uil uil-dribbble"></i></a>
                        <a href="#"><i class="uil uil-instagram"></i></a>
                        <a href="#"><i class="uil uil-youtube"></i></a>
                    </nav>
                </div>
            </div>
            <div class="col-md-4 col-lg-3">
                <div class="widget">
                    <h4 class="widget-title text-white mb-3">Alamat Kami</h4>
                    <address class="pe-xl-15 pe-xxl-17">Gedung Balaikota - Jalan Medan Merdeka Selatan No. 8-9, Blok G -
                        Lantai 20/21
                    </address>
                    <a href="#" class="link-body">guru.indonesia@gmail.com</a><br/> 0812-3737-6068
                </div>
            </div>
            <div class="col-md-4 col-lg-3">
                <div class="widget">
                    <h4 class="widget-title text-white mb-3">Informasi Lainnya</h4>
                    <ul class="list-unstyled text-reset mb-0">
                        <li><a href="#">Perjalanan Komunitas</a></li>
                        <li><a href="#">Daftar Kegiatan</a></li>
                        <li><a href="#">Syarat & Ketentuan</a></li>
                        <li><a href="#">Kebijakan Kami</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12 col-lg-3">
                <div class="widget">
                    <h4 class="widget-title text-white mb-3">Informasi Terbaru Kami</h4>
                    <p class="mb-5">Dapatkan informasi terbaru Kami dengan mengisi form email dibawah untuk
                        berlangganan.</p>
                    <div class="newsletter-wrapper">
                        <div id="mc_embed_signup2">
                            <form action="https://elemisfreebies.us20.list-manage.com/subscribe/post?u=aa4947f70a475ce162057838d&amp;id=b49ef47a9a"
                                  method="post" id="mc-embedded-subscribe-form2" name="mc-embedded-subscribe-form"
                                  class="validate " target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll2">
                                    <div class="mc-field-group input-group form-floating">
                                        <input type="email" value="" name="EMAIL" class="required email form-control"
                                               placeholder="Email Address" id="mce-EMAIL2">
                                        <label for="mce-EMAIL2">Alamat Email Anda ...</label>
                                        <input type="submit" value="Join" name="subscribe" id="mc-embedded-subscribe2"
                                               class="btn btn-primary ">
                                    </div>
                                    <div id="mce-responses2" class="clear">
                                        <div class="response" id="mce-error-response2" style="display:none"></div>
                                        <div class="response" id="mce-success-response2" style="display:none"></div>
                                    </div>
                                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input
                                                type="text" name="b_ddc180777a163e0f9f66ee014_4b1bcfa0bc" tabindex="-1"
                                                value=""></div>
                                    <div class="clear"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="progress-wrap">
    <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
        <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98"/>
    </svg>
</div>

<div class="modal-general-wrapper"></div>

<div class='container-loading d-none'>
    <div class='loader'>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--text'></div>
        <div class='loader--desc'></div>
    </div>
</div>

@include('front.account.login')
@include('front.account.daftar')
@include('front.account.forgot')
<script src="{{ asset('front/js/bootstrap.5.1.3.min.js') }}"></script>
<script src="{{ asset('front/js/jquery.3.6.0.min.js') }}"></script>
<script src="{{ asset('front/js/plugins.js') }}"></script>
<script src="{{ asset('front/js/theme.js') }}"></script>
<script src="{{ asset('front/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('front/js/front.js') }}"></script>
@yield('js')
</body>

</html>