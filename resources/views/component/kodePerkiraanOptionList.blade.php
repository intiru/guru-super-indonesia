@foreach($master_parent as $r)
    <optgroup label="{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}">
        @foreach($r->childs as $r2)
            <option value="{{ $r2->master_id }}" {{ count($r2->childs) > 0 ? 'disabled=""':'' }}>
                {{ $r2->mst_kode_rekening.' - '.$r2->mst_nama_rekening }}
            </option>
            @foreach($r2->childs as $r3)
                <option value="{{ $r3->master_id }}"  {{ count($r3->childs) > 0 ? 'disabled=""':'' }}>
                    {!! $space_1.$r3->mst_kode_rekening.' - '.$r3->mst_nama_rekening  !!}
                </option>
                @foreach($r3->childs as $r4)
                    <option value="{{ $r4->master_id }}">{!! $space_1.$space_1.$r4->mst_kode_rekening.' - '.$r4->mst_nama_rekening  !!}</option>
                @endforeach
            @endforeach
        @endforeach
    </optgroup>
@endforeach