<?php

namespace app\Mail;

use app\Helpers\Main;
use app\Models\mAppointment;
use app\Models\mGuru;
use app\Models\mReminderSetting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    private $email;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $guru = mGuru::where('gru_email', $this->email)->first();
        $data = [
            'guru' => $guru
        ];

        return $this
            ->subject('Reset Password - Guru Super Indonesia')
            ->from($address = 'guru.super.indonesia@gmail.com', $name = 'Guru Super Indonesia')
            ->view('front.account.mail_reset_password')
            ->with($data);
    }
}
