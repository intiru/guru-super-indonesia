<?php

namespace app\Mail;

use app\Helpers\Main;
use app\Models\mAppointment;
use app\Models\mGuru;
use app\Models\mReminderSetting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterVerifikasi extends Mailable
{
    use Queueable, SerializesModels;

    private $id_guru;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id_guru)
    {
        $this->id_guru = $id_guru;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $guru = mGuru::where('id_guru', $this->id_guru)->first();
        $data = [
            'guru' => $guru
        ];

        return $this
            ->subject('Verifikasi Register - Guru Super Indonesia')
            ->from($address = 'guru.super.indonesia@gmail.com', $name = 'Guru Super Indonesia')
            ->view('front.account.mail_register_verifikasi')
            ->with($data);
    }
}
