<?php

namespace app\Helpers;

use app\Models\mDetailProduksi;
use app\Models\mKomposisiProduk;
use app\Models\mPengemasProduk;
use app\Models\mProduksi;
use app\Models\mStokBahan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Config;

use app\Models\mBahan;
use app\Models\mProduk;
use app\Models\mLokasi;

//use app\Models\mKategoriProduk;
use app\Models\mStokProduk;
use app\Models\mPoBahanDetail;
use app\Models\mPoBahan;


class hForum
{

    public static function forum_comment_options($row_comment, $id_feed_comment = '')
    {
        $id_guru = Session::get('guru_data')['id_guru'];
        $data = [
            'row' => $row_comment,
            'id_guru' => $id_guru,
            'id_feed_comment' => $id_feed_comment
        ];

        return view('front.forum.forum_comment_options', $data)->render();
    }

    public static function forum_feed_options($row_feed, $id_feed = '')
    {
        $id_guru = Session::get('guru_data')['id_guru'];
        $data = [
            'feed' => $row_feed,
            'id_guru' => $id_guru,
            'id_feed' => $id_feed
        ];

        return view('front.forum.forum_feed_options', $data)->render();

    }

    public static function guru_avatar_forum($gru_avatar)
    {
        if ($gru_avatar) {
            return asset('upload/' . $gru_avatar);
        } else {
            return asset('upload/user-profile.jpg');
        }
    }

    public static function forum_feed_avatar($feed)
    {
        if($feed->id_guru) {
            return hForum::guru_avatar_forum($feed->gru_avatar);
        } elseif($feed->id_admin) {
            return hForum::guru_avatar_forum($feed->foto_karyawan);
        } else {
            return asset('upload/user-profile.jpg');
        }
    }

    public static function forum_feed_name($feed)
    {
        if($feed->id_guru) {
            return $feed->gru_nama_depan.' '.$feed->gru_nama_belakang;
//            return 'Guru';
        } elseif($feed->id_admin) {
            return $feed->nama_karyawan;
//            return 'Karyawan';
        } else {
            return 'Untitled';
        }
    }

}
