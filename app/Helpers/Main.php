<?php

namespace app\Helpers;

use app\Models\mAppointment;
use app\Models\mGuru;
use app\Models\mPayment;
use app\Models\mReminderMessage;
use app\Models\mReminderSetting;
use app\Models\mUser;
use app\Models\mWhatsApp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Config;

use app\Models\Widi\mBahan;
use app\Models\Widi\mProduk;
use app\Models\Widi\mLokasi;
use app\Models\Widi\mStokProduk;
use Illuminate\View\View;


class Main
{
    public static $date_format_view = 'd F Y H:i';
    public static $error = 'error';
    public static $success = 'success';
    public static $get = 'Success getting data';
    public static $store = 'Storing data success';
    public static $update = 'Updating data success';
    public static $delete = 'Removing data success';
    public static $import = 'Importing data success';
    public static $website_official = 'http://rumahsunatbali.com';


    public static function data($breadcrumb = array(), $menuActive = '')
    {
        $user = Session::get('user');
        $user_role = Session::get('user_role');
//        $users = mUser::where('id', $user->id)->with('user_role')->first();

        $user_foto = $user->karyawan->foto_karyawan;
        $user_nama = $user->karyawan->nama_karyawan;
        $user_email = $user->karyawan->email_karyawan;

        $whatsapp = mWhatsApp::orderBy('id_whatsapp', 'DESC')->first();

        $data['menu'] = Main::generateTopMenu($menuActive);
        $data['menuAction'] = Main::menuActionData($menuActive);
        $data['footer'] = Main::footer();
        $data['metaTitle'] = Main::metaTitle($breadcrumb);
        $data['pageTitle'] = Main::pageTitle($breadcrumb);
        $data['breadcrumb'] = Main::breadcrumb($breadcrumb);
        $data['user'] = $user;
        $data['user_role_data'] = $user_role['role_akses'];
        $data['user_role_name'] = $user_role['role_name'];
        $data['user_foto'] = $user_foto;
        $data['user_nama'] = $user_nama;
        $data['user_email'] = $user_email;
        $data['pageMethod'] = '';
        $data['no'] = 1;
        $data['imgWidth'] = 100;
        $data['decimalStep'] = '.01';
        $data['roundDecimal'] = 2;
        $data['total_chat_send'] = $whatsapp->total_chat_send;
        $data['total_chat_quote'] = $whatsapp->total_chat_quote;

        $data['cons'] = Config::get('constans');

        return $data;
    }

    public static function front()
    {
        if (Session::has('guru_data')) {
            $id_guru = Session::get('guru_data')['id_guru'];
            $guru = mGuru::where('id_guru', $id_guru)->first();
        } else {
            $guru = [];
        }

        $data = [
            'front_guru' => $guru
        ];

        return $data;
    }

    public static function guru_avatar_path()
    {
        $id_guru = Session::get('guru_data')['id_guru'];
        $guru = mGuru::where('id_guru', $id_guru)->first();

        if ($guru->gru_avatar) {
            if($guru->gru_login_type == 'google') {
                return $guru->gru_avatar;
            } else {
                return asset('upload/' . $guru->gru_avatar);
            }
        } else {
            return asset('upload/user-profile.jpg');
        }
    }

    public static function companyInfo()
    {
        $data = [
            'bankType' => Config::get('constants.bankType'),
            'bankRekening' => Config::get('constants.bankRekening'),
            'bankAtasNama' => Config::get('constants.bankAtasNama'),
            'companyName' => Config::get('constants.companyName'),
            'companyAddress' => Config::get('constants.companyAddress'),
            'companyPhone' => Config::get('constants.companyPhone'),
            'companyTelp' => Config::get('constants.companyTelp'),
            'companyEmail' => Config::get('constants.companyEmail'),
            'companyBendahara' => Config::get('constants.companyBendahara'),
            'companyTuju' => Config::get('constants.companyTuju'),
        ];

        $data = (object)$data;

        return $data;
    }

    public static function response($status = 'error', $message = 'Empty', $data = NULL, $errors = NULL)
    {
        return [
            'status' => $status,
            'message' => $message,
            'data' => $data,
            'errors' => $errors
        ];
    }

    public static function access_menu_first()
    {
        $user_role = Session::get('user_role')->role_akses;
        $user_role = json_decode($user_role, TRUE);
        $first_menu_active_route = '';
        $cons = Config::get('constants.topMenu');
        $main_menu = Main::menuAdministrator();

        foreach ($user_role as $key => $row) {
            if ($row['akses_menu']) {
                $first_menu_active_route = $main_menu[$cons[$key]]['route'];
                break;
            }
        }

        return $first_menu_active_route;
    }

    public static function generateTopMenu($menuActive)
    {
//        $totalAlertBahan = Main::totalAlertBahan();
//        $totalAlertProduk = Main::totalAlertProduk();

        $data['routeName'] = Route::currentRouteName();
        $data['menu'] = Main::menuList();
        $data['menuActive'] = $menuActive;
        $data['consMenu'] = Config::get('constants.topMenu');
//        $data['badgeAlertBahan'] = Main::badgeAlertBahan($totalAlertBahan);
//        $data['badgeAlertProduk'] = Main::badgeAlertProduk($totalAlertProduk);
//        $data['badgeInventory'] = Main::bagdeInventory($totalAlertBahan + $totalAlertProduk);
//

        return view('component/menu', $data);
    }

    public static function footer()
    {
        $data = [];
        return view('component/footer', $data);
    }

    public static function format_money($number)
    {
        if (Main::check_decimal($number)) {
            return 'Rp. ' . number_format($number, 2, ',', '.');
        } else {
            return 'Rp. ' . number_format($number, 2, ',', '.');
        }
    }

    public static function unformat_money($number)
    {
        $number = str_replace(['Rp', ''], ['.', ''], [',', '.'], $number);
        $number = self::format_decimal($number);
        return self::format_number($number);
    }

    public static function format_number($number)
    {
        if (Main::check_decimal($number)) {
            return number_format($number, 2, '.', ',');
        } else {
            return number_format($number, 0, '', ',');
        }
    }

    public static function format_number_system($number)
    {
        return number_format($number, 2, ',', '.');
    }

    public static function format_number_db($number)
    {
        $number = str_replace(['.', ','], ['.', ''], $number);
        return number_format($number, 2, '.', '');
    }

    public static function format_decimal($number)
    {
        $number = str_replace(['.', ','], ['.', ''], $number);
        return number_format($number, 0, ',', '.');
    }

    public static function format_discount($number)
    {
        return $number . ' %';
    }

    public static function format_date($date)
    {
        return date('d-m-Y', strtotime($date));
    }

    public static function format_datetime_input($date)
    {
        return date('d-m-Y H:i', strtotime($date));
    }

    public static function date()
    {
        return date('Y-m-d');
    }

    public static function datetime()
    {
        return date('Y-m-d H:i:s');
    }

    public static function format_date_label($date)
    {
        return date('d F Y', strtotime($date));
    }

    public static function format_date_db($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public static function format_datetime_db($date)
    {
        return date('Y-m-d H:i:s', strtotime($date));
    }

    public static function format_datetime($date)
    {
        return date('d F Y H:i:s', strtotime($date));
    }

    public static function format_datetime_2($date)
    {
        return date('d-m-Y H:i:s', strtotime($date));
    }

    public static function format_time_db($time)
    {
        return date('H:i:s', strtotime($time));
    }

    public static function format_time_label($time)
    {
        return date('h:i A', strtotime($time));
    }

    public static function format_age($date)
    {
        $year = date_diff(date_create($date), date_create('today'))->y;
        $month = date_diff(date_create($date), date_create('today'))->m;
        $label = $year . ' Tahun ' . $month . ' Bulan';

        return $label;
    }

    public static function age($date)
    {
        $year = date_diff(date_create($date), date_create('today'))->y;
        $month = date_diff(date_create($date), date_create('today'))->m;
        $label = $year;

        return $label;
    }


    public static function convert_money($str)
    {
        $find = array('Rp', '.', '_', ' ');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function encrypt($id)
    {
        return Crypt::encrypt($id);
    }

    public static function decrypt($id)
    {
        return Crypt::decrypt($id);
    }

    public static function convert_number($str)
    {
        $find = array('.', '_', ' ');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function convert_discount($str)
    {
        $find = array('%', ' ', '_');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function check_decimal($number)
    {
        if ($number - floor($number) >= 0.1) {
            return true;
        } else {
            return false;
        }
    }

    public static function metaTitle($breadcrumb)
    {
        krsort($breadcrumb);
        $title = '';
        foreach ($breadcrumb as $label => $value) {
            $title .= Main::menuAction($value['label']) . ' < ';
        }

        $title .= env("APP_NAME", "Sistem Klinik");

        return $title;

    }

    public static function pageTitle($breadcrumb = array())
    {
        krsort($breadcrumb);
        $title = isset($breadcrumb[1]) ? Main::menuAction($breadcrumb[1]['label']) : Main::menuAction($breadcrumb[0]['label']);

        return $title;

    }

    public static function menuAction($string)
    {
        $string = str_replace('_', ' ', $string);
        $string = ucwords($string);

        return $string;
    }

    public static function menuStrip($string)
    {
        return str_replace([' ', '/'], '_', strtolower($string));
    }

    public static function menuActionData($menuActive)
    {
        $menuList = Main::menuAdministrator();
        $routeName = Route::currentRouteName();
        $userRole = json_decode(Session::get('user.user_role.role_akses'), TRUE);
        //$menuActive = '';
        $action = [];

        if ($menuActive == '') {
            foreach ($menuList as $menu => $val) {
                if ($val['route'] == $routeName) {
                    $menuActive = $menu;
                    break;
                } else {
                    if (isset($val['sub'])) {
                        foreach ($val['sub'] as $menu_sub => $val_sub) {
                            if ($val_sub['route'] == $routeName) {
                                $menuActive = $menu_sub;
                            }
                        }
                    }
                }
            }
        }
        if ($userRole) {
            foreach ($userRole as $menuName => $menuVal) {
                if ($menuName == $menuActive) {
                    $action = $menuVal;
                } else {
                    foreach ($menuVal as $menuSubName => $menuSubVal) {
                        if ($menuSubName == $menuActive) {
                            $action = $menuSubVal;
                        }
                    }
                }
            }
        }

        return $action;

    }

    public static function string_to_number($text)
    {
        return str_replace(',', '', $text);
    }

    public static function breadcrumb($breadcrumb_extend = array())
    {
        $cons = Config::get('constants.topMenu');

        $breadcrumb[] = [
            'label' => $cons['dashboard'],
            'route' => route('dashboardPage')
        ];

        $data['breadcrumb'] = array_merge($breadcrumb, $breadcrumb_extend);
        return view('component.breadcrumb', $data);

    }

    public static function short_desc($string)
    {
        return substr(strip_tags($string), 0 , 200).'...';
    }

    /**
     * @return array
     *
     * Menu variable
     */
    public static function menuList()
    {

        return Main::menuAdministrator();

    }

    public static function menuAdministrator()
    {
        $cons = Config::get('constants.topMenu');

        return [
            $cons['dashboard'] => [
                'icon' => 'flaticon-dashboard',
                'route' => 'dashboardPage',
                'action' => ['list']
            ],
//            $cons['feed_admin'] => [
//                'icon' => 'flaticon-cogwheel-2',
//                'route' => 'backFeedAdminList',
//                'action' => [
//                    'list',
//                    'create_admin',
//                    'create_member',
//                    'detail',
//                    'edit',
//                    'delete'
//                ]
//            ],
            $cons['guru'] => [
                'icon' => 'flaticon-cogwheel-2',
                'route' => 'backGuruList',
                'action' => [
                    'list',
                    'create_admin',
                    'create_member',
                    'detail',
                    'edit',
                    'delete'
                ]
            ],
            $cons['data_blog'] => [
                'icon' => 'flaticon-cogwheel-2',
                'route' => 'dashboardPage',
                'sub' => [
                    $cons['kategori_blog'] => [
                        'route' => 'blogCategoryList',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['blog'] => [
                        'route' => 'blogList',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                ],
            ],

            $cons['masterData'] => [
                'icon' => 'flaticon-cogwheel-2',
                'route' => '#',
                'sub' => [
                    $cons['master_1'] => [
                        'route' => 'karyawanPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_2'] => [
                        'route' => 'userRolePage',
                        'action' => [
                            'list',
                            'menu_akses',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_3'] => [
                        'route' => 'userPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_4'] => [
                        'route' => 'actionMethodPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                ],
            ]
        ];
    }

    public static function menuDistributor()
    {
        $cons = Config::get('constants.topMenu');
        return [
            $cons['dashboard'] => [
                'icon' => 'flaticon-dashboard',
                'route' => 'dashboardPage',
            ],
            $cons['transaksi_3'] => [
                'icon' => 'flaticon-arrows',
                'route' => 'orderOnlinePage'
            ]
        ];
    }

    public static function scheduleCalendarModal()
    {
        $appointment = mAppointment::with('patient')->get();
        $data = [
            'appointment' => $appointment
        ];

        $css = '
        <link href="' . asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') . '" rel="stylesheet"
          type="text/css"/>
          ';

        $js = '
    <script src="' . asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') . '"
            type="text/javascript"></script>
            ';

        $js .= view('scheduleCalendar/scheduleCalendar/scheduleCalendarJs', $data);

        $data = [
            'css' => $css,
            'js' => $js,
            'view' => view('scheduleCalendar/scheduleCalendar/scheduleCalendarModal')
        ];

        return $data;
    }


    public static function day_format_id($day)
    {
        switch ($day) {
            case "Sunday":
                return 'Minggu';
                break;
            case "Monday":
                return 'Senin';
                break;
            case "Tuesday":
                return 'Selasa';
                break;
            case "Wednesday":
                return 'Rabu';
                break;
            case "Thursday":
                return 'Kamis';
                break;
            case "Friday":
                return 'Jumat';
                break;
            case "Saturday":
                return 'Sabtu';
                break;
        }
    }

    public static function whatsappSend($number, $message)
    {
//        $apikey = 604507;
//        $message = urlencode($message);
//        $client = new \GuzzleHttp\Client();
//        $request = $client->get('https://api.callmebot.com/whatsapp.php?phone='.$nomer.'&text='.$message.'&apikey='.$apikey);
//        $response = $request->getBody()->getContents();
//        echo '<pre>';
//        print_r($response);
//        exit;


//        $chatApiToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk4MTM2NTcsInVzZXIiOiI2MjgxOTM0MzY0MDYzIn0.YEq5LzdGO1vpbGw6Xle9SJo_qFVs5WfM7AZmOTaO4rs"; // Get it from https://www.phphive.info/255/get-whatsapp-password/
//
////        $number = "+6281934364063"; // Number
////        $message = "Hello :)"; // Message
//
//        $curl = curl_init();
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => 'http://chat-api.phphive.info/message/send/text',
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => '',
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 0,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => 'POST',
//            CURLOPT_POSTFIELDS =>json_encode(array("jid"=> $number."@s.whatsapp.net", "message" => $message)),
//            CURLOPT_HTTPHEADER => array(
//                'Authorization: Bearer '.$chatApiToken,
//                'Content-Type: application/json'
//            ),
//        ));
//
//        $response = curl_exec($curl);
//        curl_close($curl);
//        echo $response;
//
//        echo $number.'<br  />';
//        echo $message;


        $apikey = 'ZIAWMPS5ZTVJLMGALBW3'; // api key nomer, rumah sunat bali
        $message = urlencode($message);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://panel.rapiwha.com/send_message.php?apikey=" . $apikey . "&number=" . $number . "&text=" . $message,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }


    }

    /**
     * @param null $message_type
     * @param null $appointment
     * @param null $patient
     * @return mixed
     */
    public static function whatsapp_message($message_type = NULL, $appointment = NULL, $patient = NULL)
    {
        $message_template = mReminderMessage::where('type', $message_type)->value('message');


        $salam_pembuka = self::greating();
        $jam_appointment = $appointment ? Main::format_time_label($appointment->appointment_time) : '';
        $hari_appointment = $appointment ? Main::day_format_id(date('l', strtotime($appointment->appointment_time))) : '';
        $tanggal_appointment = $appointment ? Main::format_date($appointment->appointment_time) : '';
        $tahap_kontrol = $appointment ? $appointment->control_step : '';

        $jumlah_follow_up_jangka_pendek = mReminderSetting::where('variable', 'reminder_followup_short')->value('target_day');
        $jumlah_follow_up_jangka_panjang = mReminderSetting::where('variable', 'reminder_followup_long')->value('target_day');

        $nama_pasien = $patient ? $patient->name : '';
        $tanggal_ulang_tahun_pasien = $patient ? $patient->birthday : '';
        $umur_pasien = $patient ? Main::age($patient->birthday) : '';
        $berat_pasien = $patient ? $patient->weight : '';

        $find = [
            '{salam_pembuka}',
            '{jam_appointment}',
            '{hari_appointment}',
            '{tanggal_appointment}',
            '{jumlah_follow_up_jangka_pendek}',
            '{jumlah_follow_up_jangka_panjang}',
            '{nama_pasien}',
            '{tanggal_ulang_tahun_pasien}',
            '{umur_pasien}',
            '{berat_pasien}',
            '{tahap_kontrol}',
        ];

        $replace = [
            $salam_pembuka,
            $jam_appointment,
            $hari_appointment,
            $tanggal_appointment,
            $jumlah_follow_up_jangka_pendek,
            $jumlah_follow_up_jangka_panjang,
            $nama_pasien,
            $tanggal_ulang_tahun_pasien,
            $umur_pasien,
            $berat_pasien,
            $tahap_kontrol
        ];

        return str_replace($find, $replace, $message_template);


    }

    public static function file_rename($filename)
    {
        $datetime = date('Y_m_d__H_i_s__');
        $slug = str_replace(' ', '_', strtolower($filename));
        return $datetime . $slug;
    }

}
