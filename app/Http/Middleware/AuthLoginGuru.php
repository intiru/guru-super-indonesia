<?php

namespace app\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class AuthLoginGuru
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $login = Session::get('guru_login');
        if (!$login) {
            return redirect()->route('frontBeranda');
        }

        return $next($request);
    }
}
