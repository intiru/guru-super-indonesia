<?php

namespace app\Http\Controllers\Front;

use app\Models\mCity;
use app\Models\mFeed;
use app\Models\mFeedComment;
use app\Models\mFeedReact;
use app\Models\mProvince;
use app\Models\mSubdistrict;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use app\Helpers\hForum;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;
use Illuminate\Support\Facades\Session;

class Forum extends Controller
{

    function __construct()
    {

    }

    function list()
    {
        $data = [];
        $data = array_merge(Main::front(), $data);

        if (Session::get('guru_login')) {
            $data['feed_list'] = mFeed
                ::with([
                    'feed_comment' => function ($query) {
                        $query
                            ->where('id_feed_comment_parent', 0)
                            ->orderBy('id_feed_comment', 'ASC');
                    },
                ])
                ->select([
                    'feed.*',
                    'guru.gru_nama_depan', 'guru.gru_nama_belakang', 'guru.gru_avatar',
                    'tb_user.id', 'tb_user.id_karyawan',
                    'tb_karyawan.nama_karyawan', 'tb_karyawan.foto_karyawan'
                ])
                ->leftJoin('guru', 'guru.id_guru', '=', 'feed.id_guru')
                ->leftJoin('tb_user', 'tb_user.id', '=', 'feed.id_admin')
                ->leftJoin('tb_karyawan', 'tb_karyawan.id', '=', 'tb_user.id_karyawan')
                ->orderBy('id_feed', 'DESC')
                ->get();

            $data['feed_top'] = mFeed
                ::where('fed_status', 'on')
                ->skip(0)
                ->limit(5)
                ->orderBy('fed_comment_count', 'DESC')
                ->get();

            return view('front.forum.forum_list', $data);
        } else {
            return view('front.forum.forum_list_placeholder', $data);
        }
    }

    function guru_feed(Request $request)
    {
        $request->validate([
            'feed' => 'required'
        ]);

        $id_guru = Session::get('guru_data')['id_guru'];
        $feed = $request->input('feed');

        $data_insert = [
            'id_guru' => $id_guru,
            'fed_isi' => $feed,
            'fed_status' => 'on'
        ];

        if ($request->hasFile('gambar')) {
            $gambar = $request->file('gambar');
            $fed_gambar = Main::file_rename($gambar->getClientOriginalName());
            $gambar->move('upload/', $fed_gambar);
            $data_insert['fed_gambar'] = $fed_gambar;
        }

        mFeed::create($data_insert);
    }

    function guru_feed_delete(Request $request)
    {
        $id_feed = $request->input('id_feed');
        $id_feed = Main::decrypt($id_feed);
        $fed_gambar = mFeed::where('id_feed', $id_feed)->value('fed_gambar');
        if ($fed_gambar) {
            File::delete('upload/' . $fed_gambar);
        }

        mFeed::where('id_feed', $id_feed)->delete();
    }

    function guru_feed_edit_modal(Request $request)
    {
        $id_feed = $request->input('id_feed');
        $id_feed = Main::decrypt($id_feed);
        $feed = mFeed::where('id_feed', $id_feed)->first();

        $data = Main::front();
        $data['feed'] = $feed;

        return view('front.forum.forum_feed_edit_modal', $data);
    }

    function guru_feed_update(Request $request, $id_feed)
    {

        $request->validate([
            'feed' => 'required'
        ]);

        $id_feed = Main::decrypt($id_feed);
        $feed = $request->input('feed');

        $data_update = [
            'fed_isi' => $feed,
        ];

        if ($request->hasFile('gambar')) {
            $gambar = $request->file('gambar');
            $fed_gambar = Main::file_rename($gambar->getClientOriginalName());
            $gambar->move('upload/', $fed_gambar);
            $data_update['fed_gambar'] = $fed_gambar;

            $fed_gambar_old = mFeed::where('id_feed', $id_feed)->value('fed_gambar');
            if ($fed_gambar_old) {
                File::delete('upload/' . $fed_gambar_old);
            }
        }

        mFeed::where('id_feed', $id_feed)->update($data_update);

    }

    function guru_feed_like(Request $request)
    {
        $id_feed = $request->input('id_feed');
        $id_feed = Main::decrypt($id_feed);
        $id_guru = Session::get('guru_data')['id_guru'];
        $liked = TRUE;

        $check = mFeedReact
            ::where([
                'id_feed' => $id_feed,
                'id_guru' => $id_guru
            ])
            ->count();

        if ($check > 0) {
            $liked = FALSE;
            mFeedReact
                ::where([
                    'id_feed' => $id_feed,
                    'id_guru' => $id_guru
                ])
                ->delete();
        } else {
            mFeedReact
                ::where([
                    'id_feed' => $id_feed,
                    'id_guru' => $id_guru
                ])
                ->forceDelete();

            mFeedReact::create([
                'id_feed' => $id_feed,
                'id_guru' => $id_guru,
                'fdr_tipe' => 'like'
            ]);
        }

        $total_like = mFeedReact::where('id_feed', $id_feed)->count();
        mFeed
            ::where('id_feed', $id_feed)
            ->update([
                'fed_react_like_count' => $total_like
            ]);

        return [
            'liked' => $liked,
            'total_like' => $total_like
        ];


    }


    function guru_comment(Request $request)
    {
        $request->validate([
            'isi' => 'required'
        ]);

        $fdc_isi = $request->input('isi');
        $id_guru = Session::get('guru_data')['id_guru'];
        $id_feed = $request->input('id_feed');
        $id_feed = Main::decrypt($id_feed);
        $id_feed_comment_parent = $request->input('id_feed_comment_parent');
        $id_feed_comment_parent = Main::decrypt($id_feed_comment_parent);

        $data_insert = [
            'id_feed_comment_parent' => $id_feed_comment_parent,
            'id_feed' => $id_feed,
            'id_guru' => $id_guru,
            'fdc_isi' => $fdc_isi,
        ];

        mFeedComment::create($data_insert);

        $fed_comment_count = mFeedComment::where('id_feed', $id_feed)->count();
        mFeed::where('id_feed', $id_feed)->update(['fed_comment_count' => $fed_comment_count]);


    }

    function guru_comment_update(Request $request)
    {
        $request->validate([
            'isi' => 'required'
        ]);

        $fdc_isi = $request->input('isi');
        $id_feed_comment = $request->input('id_feed_comment');
        $id_feed_comment = Main::decrypt($id_feed_comment);

        $data_update = [
            'fdc_isi' => $fdc_isi,
        ];

        mFeedComment::where('id_feed_comment', $id_feed_comment)->update($data_update);

    }

    function guru_comment_delete(Request $request)
    {
        $request->validate([
            'id_feed_comment' => 'required'
        ]);

        $id_feed_comment = $request->input('id_feed_comment');
        $id_feed_comment = Main::decrypt($id_feed_comment);
        $id_feed = mFeedComment::where('id_feed_comment', $id_feed_comment)->value('id_feed');

        /**
         * Proses delete feed comment, sampai ke child nya
         */
        $id_feed_comment_arr = [];
        $id_feed_comment_arr[] = $id_feed_comment;
        $feed_comment_1 = mFeedComment::where('id_feed_comment_parent', $id_feed_comment)->get();
        foreach ($feed_comment_1 as $row_1) {
            $id_feed_comment_arr[] = $row_1->id_feed_comment;
            $feed_comment_2 = mFeedComment::where('id_feed_comment_parent', $row_1->id_feed_comment)->get();
            foreach ($feed_comment_2 as $row_2) {
                $id_feed_comment_arr[] = $row_2->id_feed_comment;
                $feed_comment_3 = mFeedComment::where('id_feed_comment_parent', $row_2->id_feed_comment)->get();
                foreach ($feed_comment_3 as $row_3) {
                    $id_feed_comment_arr[] = $row_3->id_feed_comment;
                    $feed_comment_4 = mFeedComment::where('id_feed_comment_parent', $row_3->id_feed_comment)->get();
                    foreach ($feed_comment_4 as $row_4) {
                        $id_feed_comment_arr[] = $row_4->id_feed_comment;
                        $feed_comment_5 = mFeedComment::where('id_feed_comment_parent', $row_4->id_feed_comment)->get();
                        foreach ($feed_comment_5 as $row_5) {
                            $id_feed_comment_arr[] = $row_5->id_feed_comment;
                            $feed_comment_6 = mFeedComment::where('id_feed_comment_parent', $row_5->id_feed_comment)->get();
                            foreach ($feed_comment_6 as $row_6) {
                                $id_feed_comment_arr[] = $row_6->id_feed_comment;
                                $feed_comment_7 = mFeedComment::where('id_feed_comment_parent', $row_6->id_feed_comment)->get();
                                foreach ($feed_comment_7 as $row_7) {
                                    $id_feed_comment_arr[] = $row_7->id_feed_comment;
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($id_feed_comment_arr as $id_feed_comment) {
            mFeedComment::where('id_feed_comment', $id_feed_comment)->delete();
        }


        $fed_comment_count = mFeedComment::where('id_feed', $id_feed)->count();
        mFeed::where('id_feed', $id_feed)->update(['fed_comment_count' => $fed_comment_count]);

    }

    function feed_saya()
    {
        $data = [];
        $data = array_merge(Main::front(), $data);

        $data['feed_list'] = mFeed
            ::with([
                'feed_comment' => function ($query) {
                    $query
                        ->where('id_feed_comment_parent', 0)
                        ->orderBy('id_feed_comment', 'ASC');
                },
            ])
            ->select([
                'feed.*',
                'guru.gru_nama_depan', 'guru.gru_nama_belakang', 'guru.gru_avatar',
                'tb_user.id', 'tb_user.id_karyawan',
                'tb_karyawan.nama_karyawan', 'tb_karyawan.foto_karyawan'
            ])
            ->where('feed.id_guru', $data['front_guru']->id_guru)
            ->leftJoin('guru', 'guru.id_guru', '=', 'feed.id_guru')
            ->leftJoin('tb_user', 'tb_user.id', '=', 'feed.id_admin')
            ->leftJoin('tb_karyawan', 'tb_karyawan.id', '=', 'tb_user.id_karyawan')
            ->orderBy('id_feed', 'DESC')
            ->get();

        $data['feed_top'] = mFeed
            ::where('fed_status', 'on')
            ->skip(0)
            ->limit(5)
            ->orderBy('fed_comment_count', 'DESC')
            ->get();

        return view('front.account.feed_saya', $data);
    }

}
