<?php

namespace app\Http\Controllers\Front;

use app\Models\mAction;
use app\Models\mCity;
use app\Models\mMedicRecord;
use app\Models\mPatient;
use app\Models\mPayment;
use app\Models\mProvince;
use app\Models\mSubdistrict;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;

class Front extends Controller
{

    function __construct()
    {

    }

    function home()
    {
        $data = [];
        $data = array_merge(Main::front(), $data);
        return view('front.front.beranda', $data);
    }

    function layanan()
    {
        $data = [];
        $data = array_merge(Main::front(), $data);
        return view('front.front.layanan', $data);
    }

    function tentang()
    {
        $data = [];
        $data = array_merge(Main::front(), $data);
        return view('front.front.tentang', $data);
    }

    function blog()
    {
        $data = [];
        $data = array_merge(Main::front(), $data);
        return view('front.front.blog', $data);
    }

    function blog_detail()
    {
        $data = [];
        $data = array_merge(Main::front(), $data);
        return view('front.front.blog_detail', $data);
    }

    function faq()
    {
        $data = [];
        $data = array_merge(Main::front(), $data);
        return view('front.front.faq', $data);
    }

    function kontak()
    {
        $data = [];
        $data = array_merge(Main::front(), $data);
        return view('front.front.kontak', $data);
    }

}
