<?php

namespace app\Http\Controllers\Front;

use app\Mail\RegisterVerifikasi;
use app\Mail\ResetPassword;
use app\Models\mCity;
use app\Models\mGuru;
use app\Models\mGuruGoogle;
use app\Models\mProvince;
use app\Models\mSubdistrict;
use app\Models\mUser;
use app\Models\mUserRole;
use app\Rules\GuruLoginCheck;
use app\Rules\RegisterEmailChecker;
use app\Rules\EmailResetPasswordChecker;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite as Socialite;

class Account extends Controller
{

    function __construct()
    {

    }

    function register_process(Request $request)
    {
        $request->validate([
            'nama_depan' => 'required',
            'nama_belakang' => 'required',
            'email' => ['required', new RegisterEmailChecker()],
            'password' => 'required',
        ]);

        $nama_depan = $request->input('nama_depan');
        $nama_belakang = $request->input('nama_belakang');
        $email = $request->input('email');
        $password = $request->input('password');

        $data_insert = [
            'gru_nama_depan' => $nama_depan,
            'gru_nama_belakang' => $nama_belakang,
            'gru_email' => $email,
            'gru_password' => Hash::make($password),
            'gru_status' => 'register'
        ];

        $id_guru = mGuru::create($data_insert)->id_guru;

        Mail::to($email)->send(new RegisterVerifikasi($id_guru));

    }

    function test_email()
    {
        $email = 'mahendra.adi.wardana@gmail.com';
        $id_guru = 6;
        Mail::to($email)->send(new RegisterVerifikasi($id_guru));
    }

    function register_verifikasi($id_guru)
    {
        $id_guru = Main::decrypt($id_guru);

        $check = mGuru::where('id_guru', $id_guru)->count();
        if ($check > 0) {
            mGuru
                ::where('id_guru', $id_guru)
                ->update([
                    'gru_status' => 'verifikasi'
                ]);
            return view('front.front.verifikasi_guru_found');
        } else {
            return view('front.front.verifikasi_guru_not_found');
        }
    }

    function profil()
    {
        $id_guru = Session::get('guru_data')['id_guru'];
        $guru = mGuru::where('id_guru', $id_guru)->first();

//        return Session::get('guru_data');

        $data = [
            'guru' => $guru,
            'step' => 2
        ];

        $data = array_merge(Main::front(), $data);

        if ($guru->gru_status == 'verifikasi') {
            return view('front/account/profil_lengkapi_step_2', $data);
        } else if ($guru->gru_status == 'active' && Session::get('profil_lengkapi_step_2') == 'done') {
            $data['step'] = 3;
            return view('front/account/profil_lengkapi_step_3', $data);
        } else {
            return view('front/account/profil', $data);
        }

    }

    function profil_update(Request $request)
    {
        $request->validate([
            'nama_depan' => 'required',
            'nama_belakang' => 'required',
            'nama_lengkap' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'alamat_kediaman' => 'required',
            'alamat_mengajar' => 'required',
            'bidang_pengajaran' => 'required',
            'kedudukan_pengajaran' => 'required',
            'mulai_bekerja' => 'required',
        ]);

        $nama_depan = $request->input('nama_depan');
        $nama_belakang = $request->input('nama_belakang');
        $nama_lengkap = $request->input('nama_lengkap');
        $tempat_lahir = $request->input('tempat_lahir');
        $tanggal_lahir = $request->input('tanggal_lahir');
        $jenis_kelamin = $request->input('jenis_kelamin');
        $alamat_kediaman = $request->input('alamat_kediaman');
        $alamat_mengajar = $request->input('alamat_mengajar');
        $bidang_pengajaran = $request->input('bidang_pengajaran');
        $kedudukan_pengajaran = $request->input('kedudukan_pengajaran');
        $mulai_bekerja = $request->input('mulai_bekerja');

        $data_update = [
            'gru_nama_depan' => $nama_depan,
            'gru_nama_belakang' => $nama_belakang,
            'gru_nama_lengkap' => $nama_lengkap,
            'gru_tempat_lahir' => $tempat_lahir,
            'gru_tanggal_lahir' => Main::format_date_db($tanggal_lahir),
            'gru_jenis_kelamin' => $jenis_kelamin,
            'gru_alamat_kediaman' => $alamat_kediaman,
            'gru_alamat_mengajar' => $alamat_mengajar,
            'gru_bidang_pengajaran' => $bidang_pengajaran,
            'gru_kedudukan_pengajaran' => $kedudukan_pengajaran,
            'gru_mulai_bekerja' => Main::format_date_db($mulai_bekerja),
            'gru_status' => 'active'
        ];

        $id_guru = Session::get('guru_data')['id_guru'];

        if (!$nama_depan && !$nama_belakang) {
            Session::put([
                'profil_lengkapi_step_2' => 'done'
            ]);
        }

        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $gru_avatar = Main::file_rename($avatar->getClientOriginalName());
            $avatar->move('upload/', $gru_avatar);

            $data_update['gru_avatar'] = $gru_avatar;

            $gru_avatar_old = mGuru::where('id_guru', $id_guru)->value('gru_avatar');
            File::delete('upload/' . $gru_avatar_old);
        }

        $guru_data = mGuru::where('id_guru', $id_guru)->first();
        Session::put([
            'guru_data' => $guru_data
        ]);


        mGuru::where('id_guru', $id_guru)->update($data_update);
    }

    function login_process(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required', new GuruLoginCheck($request->input('email'))]
        ]);

        $email = $request->input('email');

        $guru = mGuru::where('gru_email', $email)->first();
        mGuru::where('id_guru', $guru->id_guru)
            ->update([
                'gru_login_last' => date('Y-m-d H:i:s')
            ]);
        $guru = mGuru::where('gru_email', $email)->first();

        $session = [
            'guru_login' => TRUE,
            'guru_data' => $guru
        ];

        Session::put($session);
    }

    function logout_process()
    {
        Session::flush();

        return redirect()->route('frontBeranda');
    }

    function reset_password_send_email(Request $request)
    {
        $request->validate([
            'email' => [
                'required',
                'email',
                new EmailResetPasswordChecker()
            ]
        ]);

        $email = $request->input('email');

        Mail::to($email)->send(new ResetPassword($email));

    }

    function reset_password_page($id_guru)
    {
        $id_guru = Main::decrypt($id_guru);
        $guru = mGuru::where('id_guru', $id_guru)->first();
        $data = [
            'guru' => $guru
        ];

        return view('front.account.reset_password', $data);

    }

    function reset_password_process(Request $request, $id_guru)
    {
        $request->validate([
            'password' => 'required',
            'password_conf' => 'required|same:password'
        ]);

        $id_guru = Main::decrypt($id_guru);
        $password = $request->input('password');
        $password = Hash::make($password);

        mGuru::where('id_guru', $id_guru)->update(['gru_password' => $password]);
    }

    function redirect_google()
    {
        return Socialite::driver('google')->redirect();
    }

    function login_google()
    {
        $google = Socialite::driver('google')->stateless()->user();

//        return [
//            'google' => $google->user['verified_email']
//        ];

        $token = $google->token;
        $refresh_token = $google->refreshToken;
        $expires_in = $google->expiresIn;
        $id = $google->id;
        $nickname = $google->nickname;
        $name = $google->name;
        $email = $google->email;
        $avatar = $google->avatar;
        $verified_email = $google->user['verified_email'];
        $given_name = $google->user['given_name'];
        $family_name = $google->user['family_name'];
        $picture = $google->user['picture'];
        $locale = $google->user['locale'];
        $avatar_original = $google->avatar_original;

        $data_google = [
            'token' => $token,
            'refresh_token' => $refresh_token,
            'expires_in' => $expires_in,
            'id' => $id,
            'nickname' => $nickname,
            'name' => $name,
            'email' => $email,
            'avatar' => $avatar,
            'verified_email' => $verified_email,
            'given_name' => $given_name,
            'family_name' => $family_name,
            'picture' => $picture,
            'locale' => $locale,
            'avatar_original' => $avatar_original,
            'json' => json_encode($google)
        ];

        $check = mGuruGoogle::where('id', $id)->count();

        if($check == 0) {
            $data_guru = [
                'gru_nama_depan' => $given_name,
                'gru_nama_belakang' => $family_name,
                'gru_email' => $email,
                'gru_avatar' => $avatar_original,
                'gru_login_last' => Main::datetime(),
                'gru_login_type' => 'google'
            ];

            $id_guru = mGuru::create($data_guru)->id_guru;
            $data_google['id_guru'] = $id_guru;

            mGuruGoogle::create($data_google);
        } else {
            $data_guru = [
                'gru_nama_depan' => $given_name,
                'gru_nama_belakang' => $family_name,
                'gru_email' => $email,
                'gru_avatar' => $avatar_original,
                'gru_status' => 'active',
                'gru_login_last' => Main::datetime()
            ];

            $id_guru = mGuruGoogle::where('id', $id)->value('id_guru');
            mGuru::where('id_guru', $id_guru)->update($data_guru);
            $data_google['id_guru'] = $id_guru;

            mGuruGoogle::where('id', $id)->update($data_google);
        }

        $guru = mGuru::where('id_guru', $id_guru)->first();

        $session = [
            'guru_login' => TRUE,
            'guru_data' => $guru
        ];

        Session::put($session);

        return redirect()->route('frontBeranda');

    }

    function redirect_facebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    function login_facebook()
    {
        $facebook = Socialite::driver('facebook')->stateless()->user();

        return [
            'facebook' => $facebook
        ];

        $token = $google->token;
        $refresh_token = $google->refreshToken;
        $expires_in = $google->expiresIn;
        $id = $google->id;
        $nickname = $google->nickname;
        $name = $google->name;
        $email = $google->email;
        $avatar = $google->avatar;
        $verified_email = $google->user['verified_email'];
        $given_name = $google->user['given_name'];
        $family_name = $google->user['family_name'];
        $picture = $google->user['picture'];
        $locale = $google->user['locale'];
        $avatar_original = $google->avatar_original;

        $data_google = [
            'token' => $token,
            'refresh_token' => $refresh_token,
            'expires_in' => $expires_in,
            'id' => $id,
            'nickname' => $nickname,
            'name' => $name,
            'email' => $email,
            'avatar' => $avatar,
            'verified_email' => $verified_email,
            'given_name' => $given_name,
            'family_name' => $family_name,
            'picture' => $picture,
            'locale' => $locale,
            'avatar_original' => $avatar_original,
            'json' => json_encode($google)
        ];

        $check = mGuruGoogle::where('id', $id)->count();

        if($check == 0) {
            $data_guru = [
                'gru_nama_depan' => $given_name,
                'gru_nama_belakang' => $family_name,
                'gru_email' => $email,
                'gru_avatar' => $avatar_original,
                'gru_login_last' => Main::datetime(),
                'gru_login_type' => 'google'
            ];

            $id_guru = mGuru::create($data_guru)->id_guru;
            $data_google['id_guru'] = $id_guru;

            mGuruGoogle::create($data_google);
        } else {
            $data_guru = [
                'gru_nama_depan' => $given_name,
                'gru_nama_belakang' => $family_name,
                'gru_email' => $email,
                'gru_avatar' => $avatar_original,
                'gru_status' => 'active',
                'gru_login_last' => Main::datetime()
            ];

            $id_guru = mGuruGoogle::where('id', $id)->value('id_guru');
            mGuru::where('id_guru', $id_guru)->update($data_guru);
            $data_google['id_guru'] = $id_guru;

            mGuruGoogle::where('id', $id)->update($data_google);
        }

        $guru = mGuru::where('id_guru', $id_guru)->first();

        $session = [
            'guru_login' => TRUE,
            'guru_data' => $guru
        ];

        Session::put($session);

        return redirect()->route('frontBeranda');

    }

}
