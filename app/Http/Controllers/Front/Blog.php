<?php

namespace app\Http\Controllers\Front;

use app\Models\mAction;
use app\Models\mBlog;
use app\Models\mBlogCategory;
use app\Models\mCity;
use app\Models\mMedicRecord;
use app\Models\mPatient;
use app\Models\mPayment;
use app\Models\mProvince;
use app\Models\mSubdistrict;
use Barryvdh\DomPDF\Facade as PDF;
use Hashids\Hashids;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;

class Blog extends Controller
{

    function __construct()
    {

    }

    function index()
    {
        $data = Main::front();
        $blog_category = $this->blog_category_data();
        $blog_list = mBlog
            ::select([
                'blog.*',
                'blog_category.id_blog_category',
                'blog_category.blc_judul'
            ])
            ->leftJoin('blog_category', 'blog_category.id_blog_category', '=', 'blog.id_blog_category')
            ->orderBy('id_blog', 'DESC')
            ->paginate(2);

        $blog_populer = mBlog
            ::limit(3)
            ->offset(0)
            ->orderBy('blg_views', 'DESC')
            ->get();

        $data = array_merge($data, [
            'blog_category' => $blog_category,
            'blog_list' => $blog_list,
            'blog_populer' => $blog_populer
        ]);
        return view('front.front.blog', $data);
    }

    public static function blog_category($id_blog_category)
    {
        $data = Main::front();
        $blog_category = Blog::blog_category_data();
        $page = mBlogCategory::where('id_blog_category', $id_blog_category)->first();
        $blog_list = mBlog
            ::select([
                'blog.*',
                'blog_category.id_blog_category',
                'blog_category.blc_judul'
            ])
            ->leftJoin('blog_category', 'blog_category.id_blog_category', '=', 'blog.id_blog_category')
            ->where('blog.id_blog_category', $id_blog_category)
            ->orderBy('id_blog', 'DESC')
            ->paginate(2);

        $blog_populer = mBlog
            ::limit(3)
            ->offset(0)
            ->orderBy('blg_views', 'DESC')
            ->get();

        $data = array_merge($data, [
            'blog_category' => $blog_category,
            'blog_list' => $blog_list,
            'page' => $page,
            'blog_populer' => $blog_populer
        ]);
        return view('front.front.blog_category', $data);
    }

    public static function blog_detail($id_blog)
    {
        $data = Main::front();
        $blog_category = Blog::blog_category_data();
        $page = mBlog
            ::select([
                'blog.*',
                'blog_category.id_blog_category',
                'blog_category.blc_judul'
            ])
            ->leftJoin('blog_category', 'blog_category.id_blog_category', '=', 'blog.id_blog_category')
            ->where('id_blog', $id_blog)
            ->first();
        $page->blg_views = $page->blg_views + 1;
        mBlog::where('id_blog', $id_blog)->update(['blg_views' => $page->blg_views]);

        $blog_others = mBlog
            ::select([
                'blog.*',
                'blog_category.id_blog_category',
                'blog_category.blc_judul'
            ])
            ->leftJoin('blog_category', 'blog_category.id_blog_category', '=', 'blog.id_blog_category')
            ->whereNotIn('id_blog', [$id_blog])
            ->limit(5)
            ->offset(0)
            ->orderBy('id_blog', 'DESC')
            ->get();

        $blog_populer = mBlog
            ::limit(3)
            ->offset(0)
            ->orderBy('blg_views', 'DESC')
            ->get();

        $data = array_merge($data, [
            'blog_category' => $blog_category,
            'page' => $page,
            'blog_others' => $blog_others,
            'blog_populer' => $blog_populer,
        ]);
        return view('front.front.blog_detail', $data);
    }

    public static function blog_category_data()
    {
        $blog_category = mBlogCategory::orderBy('blc_judul', 'ASC')->get();
        foreach ($blog_category as $row) {
            $row->blog_total = mBlog::where('id_blog_category', $row->id_blog_category)->count();
        }

        return $blog_category;
    }

}
