<?php

namespace app\Http\Controllers\General;

use app\Models\mAction;
use app\Models\mAppointment;
use app\Models\mBlog;
use app\Models\mConsult;
use app\Models\mControl;
use app\Models\mFeed;
use app\Models\mFeedComment;
use app\Models\mFeedReact;
use app\Models\mGuru;
use app\Models\mPatient;
use app\Models\mPayment;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class Dashboard extends Controller
{
    private $breadcrumb = [
        [
            'label' => 'Dashboard',
            'route' => ''
        ]
    ];

    private $bulan = [
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'Nopember',
        '12' => 'Desember',
    ];

    function index(Request $request)
    {

        $date_range_get = $request->input('date_range');
        $date_range = explode(' - ', $date_range_get);

        $date_from = $request->date_from ? $request->date_from : date('01-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("t-m-Y", strtotime($date_from));
        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $data = Main::data($this->breadcrumb);

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_blog = mBlog::whereBetween('created_at', $where_date)->count();
        $total_feed = mFeed::whereBetween('created_at', $where_date)->count();
        $total_feed_comment = mFeedComment::whereBetween('created_at', $where_date)->count();
        $total_feed_react = mFeedReact::whereBetween('created_at', $where_date)->count();
        $total_guru = mGuru::whereBetween('created_at', $where_date)->count();

        $start = new \DateTime($date_from_db);
        $end = new \DateTime($date_to_db);
        $end = $end->modify('+1 day');
        $interval = new \DateInterval('P1D');

        $label = [];
        $period = new \DatePeriod($start, $interval, $end);

        foreach ($period as $key => $value) {
            $label[] = $value->format('Y-m-d');
        }


        $cart = [
            'label' => $label
        ];

        foreach ($period as $key => $value) {
            $date = $value->format('Y-m-d');

            $cart['data']['blog'][$date] =
                mBlog
                    ::whereDate(
                        'created_at', $date
                    )
                    ->count();

            $cart['data']['feed'][$date] =
                mFeed
                    ::whereDate(
                        'created_at', $date
                    )
                    ->count();

            $cart['data']['feed_comment'][$date] =
                mFeed
                    ::whereDate('created_at', $date)
                    ->count();

            $cart['data']['feed_react'][$date] =
                mFeedReact
                    ::whereDate('created_at', $date)
                    ->count();

            $cart['data']['guru'][$date] =
                mGuru
                    ::whereDate('created_at', $date)
                    ->count();
        }


        $data = array_merge($data, array(
            'total_blog' => $total_blog,
            'total_feed' => $total_feed,
            'total_feed_comment' => $total_feed_comment,
            'total_feed_react' => $total_feed_react,
            'total_guru' => $total_guru,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'cart' => $cart
        ));

        return view('dashboard/dashboard_admin', $data);

    }

}
