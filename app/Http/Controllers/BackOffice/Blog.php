<?php

namespace app\Http\Controllers\BackOffice;

use app\Models\mBlog;
use app\Models\mBlogCategory;
use app\Models\mFeed;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class Blog extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['data_blog'],
                'route' => ''
            ],
            [
                'label' => $cons['blog'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mBlog
            ::leftJoin('blog_category', 'blog_category.id_blog_category', '=', 'blog.id_blog_category')
            ->orderBy('id_blog', 'DESC')
            ->get();
        $blog_category = mBlogCategory::orderBy('blc_judul', 'ASC')->get();

        $data = array_merge($data, [
            'data' => $data_list,
            'blog_category' => $blog_category
        ]);

        return view('backOffice.blog.blogList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'blg_judul' => 'required',
            'blg_isi' => 'required',
            'blg_gambar'
        ]);

        $data = $request->except('_token');
        $data['id_admin'] = Session::get('user')['id'];

        $gambar = $request->file('gambar');
        $blg_gambar = Main::file_rename($gambar->getClientOriginalName());
        $gambar->move('upload/', $blg_gambar);
        $data['blg_gambar'] = $blg_gambar;

        mBlog::create($data);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mBlog::where('id_blog', $id)->first();
        $blog_category = mBlogCategory::orderBy('blc_judul', 'ASC')->get();
        $data = [
            'edit' => $edit,
            'blog_category' => $blog_category
        ];

        return view('backOffice.blog.blogEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        $blg_gambar = mBlog::where('id_blog', $id)->value('blg_gambar');
        if ($blg_gambar) {
            File::delete('upload/' . $blg_gambar);
        }

        mBlog::where('id_blog', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'blg_judul' => 'required',
            'blg_isi' => 'required'
        ]);
        $data = $request->except("_token");
        unset($data['gambar']);

        if ($request->hasFile('gambar')) {
            $gambar = $request->file('gambar');
            $blg_gambar = Main::file_rename($gambar->getClientOriginalName());
            $gambar->move('upload/', $blg_gambar);
            $data['blg_gambar'] = $blg_gambar;

            $blg_gambar_old = mBlog::where('id_blog', $id)->value('blg_gambar');
            if ($blg_gambar_old) {
                File::delete('upload/' . $blg_gambar_old);
            }
        }

        mBlog::where(['id_blog' => $id])->update($data);
    }
}
