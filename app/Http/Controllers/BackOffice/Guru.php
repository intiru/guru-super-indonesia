<?php

namespace app\Http\Controllers\BackOffice;

use app\Models\mGuru;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;

class Guru extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['guru'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mGuru
            ::orderBy('id_guru', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('backOffice/guru/guruList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required'
        ]);

        $data = $request->except('_token');
        mActionMethod::create($data);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mActionMethod::where('id_action_method', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('masterData/actionMethod/actionMethodEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mActionMethod::where('id_action_method', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'title' => 'required',
            'description' => 'required'
        ]);
        $data = $request->except("_token");
        mActionMethod::where(['id_action_method' => $id])->update($data);
    }
}
