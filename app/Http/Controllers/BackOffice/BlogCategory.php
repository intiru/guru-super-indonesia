<?php

namespace app\Http\Controllers\BackOffice;

use app\Models\mBlog;
use app\Models\mBlogCategory;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\Session;

class BlogCategory extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['data_blog'],
                'route' => ''
            ],
            [
                'label' => $cons['kategori_blog'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mBlogCategory
            ::orderBy('blc_judul', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('backOffice.blogCategory.blogCategoryList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'blc_judul' => 'required',
            'blc_isi' => 'required'
        ]);

        $data = $request->except('_token');
        $data['id_admin'] = Session::get('user')['id'];
        mBlogCategory::create($data);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mBlogCategory::where('id_blog_category', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('backOffice.blogCategory.blogCategoryEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mBlogCategory::where('id_blog_category', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'blc_judul' => 'required',
            'blc_isi' => 'required'
        ]);
        $data = $request->except("_token");
        mBlogCategory::where(['id_blog_category' => $id])->update($data);
    }
}
