<?php

namespace app\Http\Controllers\BackOffice;

use app\Models\mAction;
use app\Models\mActionMethod;
use app\Models\mFeed;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class FeedAdmin extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['feed_admin'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mFeed
            ::orderBy('id_feed', 'DESC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('backOffice.feedAdmin.feedAdminList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);

        $fed_judul = $request->input('judul');
        $fed_isi = $request->input('isi');


        $data_insert = [
            'fed_judul' => $fed_judul,
            'fed_isi' => $fed_isi,
            'id_admin' => Session::get('user')['id'],
            'fed_status' => 'on'
        ];

        if ($request->hasFile('gambar')) {
            $gambar = $request->file('gambar');
            $fed_gambar = Main::file_rename($gambar->getClientOriginalName());
            $gambar->move('upload/', $fed_gambar);

            $data_insert['fed_gambar'] = $fed_gambar;
        }

        mFeed::create($data_insert);

    }

    function delete(Request $request, $id_feed)
    {
        $id_feed = Main::decrypt($id_feed);
        mFeed::where('id_feed', $id_feed)->delete();
    }

    function status_update($id_feed, $status)
    {
        $id_feed = Main::decrypt($id_feed);
        mFeed::where('id_feed', $id_feed)->update(['fed_status' => $status]);
        return redirect()->route('backFeedAdminList');
    }

    function edit_modal($id_feed)
    {
        $id_feed = Main::decrypt($id_feed);
        $edit = mFeed::where('id_feed', $id_feed)->first();

        $data = [
            'edit' => $edit
        ];

        return view('backOffice.feedAdmin.feedAdminEditModal', $data);
    }

    function update(Request $request, $id_feed)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);

        $fed_judul = $request->input('judul');
        $fed_isi = $request->input('isi');
        $id_feed = Main::decrypt($id_feed);

        $data_update = [
            'fed_judul' => $fed_judul,
            'fed_isi' => $fed_isi,
        ];

        if ($request->hasFile('gambar')) {
            $fed_gambar_old = mFeed::where('id_feed', $id_feed)->value('fed_gambar');
            File::delete('upload/' . $fed_gambar_old);

            $gambar = $request->file('gambar');
            $fed_gambar = Main::file_rename($gambar->getClientOriginalName());
            $gambar->move('upload/', $fed_gambar);

            $data_update['fed_gambar'] = $fed_gambar;
        }

        mFeed::where('id_feed', $id_feed)->update($data_update);
    }
}
