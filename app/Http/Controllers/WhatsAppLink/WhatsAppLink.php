<?php

namespace app\Http\Controllers\WhatsAppLink;

use app\Models\mReminderSetting;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;
use Nexmo\Response;

class WhatsAppLink extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['whatsapp_link'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $reminder_setting = mReminderSetting::orderBy('id_reminder_setting', 'ASC')->get();

        $data = array_merge($data, array(
            'reminder_setting' => $reminder_setting
        ));


        return view('whatsappLink/whatsappLink/whatsappLink', $data);
    }

}
