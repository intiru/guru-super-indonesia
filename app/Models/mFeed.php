<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mFeed extends Model
{
    use SoftDeletes;

    protected $table = 'feed';
    protected $primaryKey = 'id_feed';
    protected $fillable = [
        'id_guru',
        'id_admin',
        'fed_judul',
        'fed_isi',
        'fed_gambar',
        'fed_react_like_count',
        'fed_react_love_count',
        'fed_react_haha_count',
        'fed_react_wow_count',
        'fed_react_sad_count',
        'fed_react_angry_count',
        'fed_react_total_count',
        'fed_comment_count',
        'fed_status',
    ];

    public function feed_comment()
    {
        return $this->hasMany(mFeedComment::class, 'id_feed');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
