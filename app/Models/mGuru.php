<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mGuru extends Model
{
    use SoftDeletes;

    protected $table = 'guru';
    protected $primaryKey = 'id_guru';
    protected $fillable = [
        'gru_nama_depan',
        'gru_nama_belakang',
        'gru_nama_lengkap',
        'gru_tempat_lahir',
        'gru_tanggal_lahir',
        'gru_jenis_kelamin',
        'gru_alamat_kediaman',
        'gru_alamat_mengajar',
        'gru_bidang_pengajaran',
        'gru_kedudukan_pengajaran',
        'gru_mulai_kerja',
        'gru_email',
        'gru_password',
        'gru_status',
        'gru_avatar',
        'gru_login_last',
        'gru_login_type',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
