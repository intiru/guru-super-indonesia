<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mFeedComment extends Model
{
    use SoftDeletes;

    protected $table = 'feed_comment';
    protected $primaryKey = 'id_feed_comment';
    protected $fillable = [
        'id_feed_comment_parent',
        'id_feed',
        'id_guru',
        'fdc_isi',
    ];

    public function guru() {
        return $this->belongsTo(mGuru::class, 'id_guru');
    }

    public function feed() {
        return $this->belongsTo(mFeed::class, 'id_feed');
    }

    public function feed_comment() {
        return $this->hasMany(mFeedComment::class, 'id_feed_comment_parent', 'id_feed_comment');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
