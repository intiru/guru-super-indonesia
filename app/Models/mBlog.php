<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mBlog extends Model
{
    use SoftDeletes;

    protected $table = 'blog';
    protected $primaryKey = 'id_blog';
    protected $fillable = [
        'id_blog_category',
        'id_admin',
        'blg_judul',
        'blg_gambar',
        'blg_isi',
        'blg_like_count',
        'blg_views',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
