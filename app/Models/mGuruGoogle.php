<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mGuruGoogle extends Model
{
    use SoftDeletes;

    protected $table = 'guru_google';
    protected $primaryKey = 'id_guru_google';
    protected $fillable = [
        'id_guru',
        'token',
        'refresh_token',
        'expires_in',
        'id',
        'nickname',
        'name',
        'email',
        'avatar',
        'verified_email',
        'given_name',
        'family_name',
        'picture',
        'locale',
        'avatar_original',
        'json'
    ];

    public function guru() {
        return $this->belongsTo(mGuru::class, 'id_guru');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
