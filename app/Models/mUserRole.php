<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mUserRole extends Model
{
    use SoftDeletes;

    protected $table = 'tb_user_role';
    protected $primaryKey = 'id';
    protected $fillable = [
        'role_name',
        'role_keterangan',
        'role_akses'
    ];
}
