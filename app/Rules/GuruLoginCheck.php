<?php

namespace app\Rules;

use app\Models\mGuru;
use app\Models\mUser;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use app\Models\mUserRole;
use Session;

class GuruLoginCheck implements Rule
{

    protected $email;
    protected $type;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $email = $this->email;
        $password = $value;
        $status = FALSE;

        $check = mGuru::where('gru_email', $email)->count();
        if ($check > 0) {
            $status = TRUE;
            $guru = mGuru::where('gru_email', $email)->first();

            /**
             * Login user
             */
            if (Hash::check($password, $guru->gru_password)) {
                $status = TRUE;
            } else {
                $this->type = 'guru_not_found';
            }

            if ($guru->gru_status == 'register') {
                $status = FALSE;
                $this->type = 'guru_register';
            }
        }


        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $label = '';
        if ($this->type == 'guru_not_found') {
            $label = 'Email atau Password tidak benar';
        } else if ($this->type == 'guru_register') {
            $label = 'Anda belum melakukan <strong>Verifikasi</strong>, silahkan periksa <strong>Kotak Masuk Email</strong> Anda';
        }

        return $label;
    }
}
