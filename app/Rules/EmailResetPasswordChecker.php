<?php

namespace app\Rules;

use app\Models\mGuru;
use Illuminate\Contracts\Validation\Rule;
use app\Models\mUser;

class EmailResetPasswordChecker implements Rule
{
    private $value = '';

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->value = $value;
        $count = mGuru::where('gru_email', $value)->count();
        if($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Email \"<strong>".$this->value."</strong>\" tidak tersedia";
    }
}
