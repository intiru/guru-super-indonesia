$(document).ready(function () {


    feed_reply();
    image_preview();
    feed_delete();
    feed_edit();

});

function feed_reply() {
    $('.btn-feed-reply').click(function (e) {
        e.preventDefault();

        var feed_comment_html = $('.feed-comment-html').html();

        $('.feed-comment-view').html('');
        $(this).parent('div').siblings('.feed-comment-view').html(feed_comment_html);

        feed_comment_cancel();

        return false;
    });
}

function feed_comment_cancel() {
    $('.btn-feed-comment-cancel').click(function () {
        $(this).parents('.feed-comment-view').html('');
    });
}

function image_preview() {
    $('.image-browse').change(function () {
        const file = this.files[0];
        var self = $(this);

        if (file) {
            let reader = new FileReader();
            reader.onload = function (event) {
                self.parents('form').find('.image-preview').attr('src', event.target.result);
                // self.parents('form').remove();
            };
            reader.readAsDataURL(file);
        }
    });
}

function feed_delete() {
    $('.btn-feed-delete').click(function (e) {
        e.preventDefault();
        var self = $(this);
        var remove_by_index = $(this).data('remove-by-index');
        var remove_index = $(this).data('remove-index');
        var reload = $(this).data('reload');
        var csrf_token = $('#base-value').data('csrf-token');
        var message = $(this).data('message');
        if (typeof message === "undefined") {
            message = "Yakin hapus data ini ?";
        }
        swal({
            title: "Perhatian ...",
            html: message,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Batal",
            heightAuto: false,
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    url: self.data('route'),
                    type: "delete",
                    data: {
                        _token: csrf_token
                    },
                    success: function () {
                        self.parents('.m-portlet').remove();
                    },
                    error: function (request) {
                        var title = request.responseJSON.title ? request.responseJSON.title : 'Ada yang salah';
                        swal({
                            title: title,
                            html: request.responseJSON.message,
                            type: "warning"
                        });
                    }
                });
            }
        });
        return false;
    });
}

function feed_edit() {
    $('.btn-feed-edit').click(function (e) {
        e.preventDefault();

        var self = $(this);
        var route = $(this).data('route');

        $.ajax({
            url: route,
            type: 'get',
            data: {},
            beforeSend: loading_start(),
            success: function (json) {
                loading_finish();

                $('.wrapper-modal').html(json);
                $('#modal-general').modal('show');

                form_send_wrapper();
                image_preview();
            }
        });
        return false;
    });
}