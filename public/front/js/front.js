$(document).ready(function () {

    form_send_wrapper();
    image_preview();

    /**
     * Feed
     */
    btn_forum_feed_edit();
    btn_forum_feed_delete();
    btn_forum_feed_like();

    /**
     * Feed Comment
     */
    btn_forum_comment_first();
    btn_feed_comment_reply();
    btn_feed_comment_edit();
    btn_feed_comment_delete();

});


function form_send_wrapper() {

    $('.form-send').submit(function (e) {
        e.preventDefault();

        var self = $(this);
        var val = false;
        var label_button = self.find('[type="submit"]').html();
        self.find('[type="submit"]').text('Loading ...').prop('disabled', true);

        console.log(label_button);

        var confirm = $(this).data('confirm');

        loading_start();

        if (confirm) {
            swal({
                title: "Perhatian ...",
                text: "Yakin Simpan data ini ?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya, yakin",
                cancelButtonText: "Batal",
            }).then(function (e) {
                if (e.value) {
                    form_send(self, label_button);
                }
            });
        } else {
            form_send(self, label_button);
        }
        return false;
    });
}

function form_send(self, label_button = '') {
    loading_start();
    var action = self.attr('action');
    var method = self.attr('method');

    var redirect = self.data('redirect');
    var pdf = self.data('pdf');
    var alert_show = self.data('alert-show');
    var alert_field_message = self.data('alert-field-message');

    var alert_show_success_status = self.data('alert-show-success-status');
    var alert_show_success_title = self.data('alert-show-success-title');
    var alert_show_success_message = self.data('alert-show-success-message');

    var message = '';
    var message_field = '';

    var form = self;
    var formData = new FormData(form[0]);

    $.ajax({
        url: action,
        type: method,
        data: formData,
        // async: false,
        beforeSend: function () {
            loading_start();
        },
        error: function (request, error) {
            loading_finish();
            $('.form-send').removeClass('was-validated');
            $('.form-send').addClass('was-validated');
            $('.form-control').removeClass('is-valid is-invalid');
            $('.invalid-feedback').remove();
            $.each(request.responseJSON.errors, function (key, val) {
                var type = self.find('[name="' + key + '"]').attr('type');
                self.find('[name="' + key + '"]').addClass('is-invalid');
                message_field += val[0] + '<br />';

                if (type == 'file') {
                    self.find('[name="' + key + '"]').parents('.input-group').after('<div class="form-control-feedback">' + val[0] + '</div>');
                } else {
                    self.find('[name="' + key + '"]').after('<div class="invalid-feedback">' + val[0] + '</div>');
                }
            });

            if (alert_show == true) {

                if (alert_field_message == true) {

                    if (message_field) {
                        message = message_field;
                    } else {
                        message = request.responseJSON.message;
                    }
                } else {
                    message = request.responseJSON.message;
                }
                swal.fire({
                    title: "Ada yang Salah",
                    html: message,
                    icon: "warning"
                });

            }


            $('.form-send [type="submit"]').html(label_button).prop('disabled', false);
        },
        success: function (data) {
            loading_finish();
            $('.form-send [type="submit"]').html(label_button).prop('disabled', false);
            if (alert_show_success_status) {
                swal.fire({
                    title: alert_show_success_title,
                    html: alert_show_success_message,
                    icon: 'success',
                    showDenyButton: false,
                    confirmButtonText: 'Baik',
                }).then(function (result) {
                    if (typeof redirect == 'undefined') {
                        window.location.reload();
                    } else {
                        window.location.href = redirect;
                    }
                });

            } else {
                if (typeof redirect == 'undefined') {
                    window.location.reload();
                    form_clear();
                } else {
                    if (pdf) {
                        window.open(pdf, "_blank");
                        window.location.href = redirect;
                    } else {
                        window.location.href = redirect;

                    }
                }
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
}

function form_clear() {
    $(".modal").modal("hide");
    $("input[type=text]").val('');
    $("input[type=email]").val('');
    $("input[type=password]").val('');
    $("input[type=checkbox]").prop("checked", false);
    $("textarea").val('');
    if ($('select').hasClass('m-select2')) {
        $('.m-select2').val(null).trigger('change');
    }
    $('select option:first-child').attr("selected", "selected");
}

function loading_start() {
    $('.container-loading').hide().removeClass('d-none').fadeIn('fast');
}

function loading_finish() {
    $('.container-loading').fadeOut('fast').addClass('d-none');
}

function image_preview() {
    $('.image-browse').change(function () {
        var self = $(this);

        const file = this.files[0];
        console.log(file);
        if (file) {
            let reader = new FileReader();
            reader.onload = function (event) {
                console.log(event.target.result);
                self.parents('form').find('.image-preview').attr('src', event.target.result);
            };
            reader.readAsDataURL(file);
        }
    });
}

function format_number(nStr) {
    nStr += '';
    x = nStr.split(',');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

/**
 * Forum Feed
 */

function btn_forum_feed_delete() {
    $('.btn-forum-feed-delete').click(function (e) {
        e.preventDefault();

        var id_feed = $(this).data('id-feed');
        var csrf_token = $('body').data('csrf-token');
        var route_feed_delete = $('body').data('route-feed-delete');

        swal.fire({
            title: "Perhatian ...",
            text: "Yakin Hapus Feed ini ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Batal",
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    url: route_feed_delete,
                    type: "delete",
                    data: {
                        _token: csrf_token,
                        id_feed: id_feed
                    },
                    success: function () {
                        window.location.reload();
                    },
                    error: function (request) {
                        var title = request.responseJSON.title ? request.responseJSON.title : 'Ada yang salah';
                        swal({
                            title: title,
                            html: request.responseJSON.message,
                            type: "warning"
                        });
                    }
                });
            }
        });

        return false;
    });
}

function btn_forum_feed_edit() {
    $('.btn-forum-feed-edit').click(function (e) {
        e.preventDefault();

        var id_feed = $(this).data('id-feed');
        var csrf_token = $('body').data('csrf-token');
        var route_feed_edit_modal = $('body').data('route-feed-edit-modal');

        loading_start();
        $.ajax({
            url: route_feed_edit_modal,
            type: "post",
            data: {
                _token: csrf_token,
                id_feed: id_feed
            },
            success: function (data) {
                loading_finish();
                $('.modal-general-wrapper').html(data);
                $('#modal-forum-feed-edit-modal').modal('show');


                form_send_wrapper();
                image_preview();
            },
            error: function (request) {
                var title = request.responseJSON.title ? request.responseJSON.title : 'Ada yang salah';
                swal({
                    title: title,
                    html: request.responseJSON.message,
                    type: "warning"
                });
            }
        });

        return false;
    });
}

function btn_forum_feed_like() {
    $('.btn-forum-feed-like').click(function (e) {
        e.preventDefault();

        var id_feed = $(this).parents('.feed-wrapper').data('id-feed');
        var route_feed_like = $('body').data('route-feed-like');
        var csrf_token = $('body').data('csrf-token');
        var self = $(this);

        loading_start();

        $.ajax({
            url: route_feed_like,
            type: 'post',
            data: {
                _token: csrf_token,
                id_feed: id_feed
            },
            success: function (data) {
                loading_finish();
                if (data.liked) {
                    self.addClass('feed-liked');
                } else {
                    self.removeClass('feed-liked')
                }

                $('.feed-wrapper[data-id-feed="' + id_feed + '"] .feed-react-total').html(format_number(data.total_like));
            }
        });

    });
}


/**
 * Forum Feed Comment
 */

function btn_forum_comment_first() {
    $('.btn-forum-comment-first').click(function (e) {
        var self = $(this);
        e.preventDefault();
        //
        // var forum_comment_html = $('.forum-comment-html').html();
        // var id_feed = self.parents('.card').data('id-feed');
        // var id_feed_comment_parent = self.data('id-feed-comment-parent');

        self.parents('.card').find('[name="isi"]').focus();

        // self.parents('.card').find('.forum-comment-first-wrapper').html('');
        // self.parents('.card').find('.forum-comment-first-wrapper').html(forum_comment_html);
        //
        // self.parents('.card').find('[name="id_feed"]').val(id_feed);
        // self.parents('.card').find('[name="id_feed_comment_parent"]').val(id_feed_comment_parent);
        //
        // form_send_wrapper();

        return false;
    });
}

function btn_feed_comment_reply() {
    $('.btn-feed-comment-reply').click(function () {
        var self = $(this);
        var forum_comment_html = $('.forum-comment-html').html();
        var id_feed = self.parents('.card').data('id-feed');
        var id_feed_comment = self.data('id-feed-comment');

        self.parents('.feed-comment-menu').siblings('.feed-comment-reply-input').html(forum_comment_html);
        self.parents('.feed-comment-menu').siblings('.feed-comment-reply-input').find('[name="id_feed"]').val(id_feed);
        self.parents('.feed-comment-menu').siblings('.feed-comment-reply-input').find('[name="id_feed_comment_parent"]').val(id_feed_comment);
        self.parents('.feed-comment-menu').siblings('.feed-comment-reply-input').find('[name="isi"]').focus();

        form_send_wrapper();
        btn_feed_comment_cancel();
    });
}

function btn_feed_comment_cancel() {
    $('.btn-feed-comment-cancel').click(function () {
        $(this).parents('.feed-comment-reply-input').html('');
    });
}

function btn_feed_comment_edit() {
    $('.btn-feed-comment-edit').click(function (e) {
        e.preventDefault();

        var self = $(this);
        var id_feed_comment = self.data('id-feed-comment');
        var check = $('.row [data-id-feed-comment="' + id_feed_comment + '"] .feed-comment-isi p').length;
        var forum_comment_edit_html = $('.forum-comment-edit-html').html();

        if (check == 0) {
            var fdc_isi = $('body').data('feed-comment');
        } else {
            var fdc_isi = $('.row [data-id-feed-comment="' + id_feed_comment + '"] .feed-comment-isi p').html();
            $('body').data('feed-comment', fdc_isi);
        }

        $('.row [data-id-feed-comment="' + id_feed_comment + '"] .feed-comment-isi').html(forum_comment_edit_html);

        $('.row [data-id-feed-comment="' + id_feed_comment + '"]').find('.feed-comment-input').val(fdc_isi);
        $('.row [data-id-feed-comment="' + id_feed_comment + '"]').find('[name="id_feed_comment"]').val(id_feed_comment);

        form_send_wrapper();
        btn_feed_comment_edit_cancel();

        return false;
    });
}

function btn_feed_comment_edit_cancel() {
    $('.btn-feed-comment-edit-cancel').click(function () {
        var fdc_isi = $('body').data('feed-comment');
        $(this).parents('.feed-comment-isi').html('<p>' + fdc_isi + '</p>');
    });
}

function btn_feed_comment_delete() {
    $('.btn-feed-comment-delete').click(function (e) {
        e.preventDefault();

        var id_feed_comment = $(this).data('id-feed-comment');
        var csrf_token = $('body').data('csrf-token');
        var route_route_feed_comment_delete = $('body').data('route-feed-comment-delete');

        swal.fire({
            title: "Perhatian ...",
            text: "Yakin Hapus komentar ini ? Komentar di dalam komentar ini akan ikut terhapus",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya, yakin",
            cancelButtonText: "Batal",
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    url: route_route_feed_comment_delete,
                    type: "delete",
                    data: {
                        _token: csrf_token,
                        id_feed_comment: id_feed_comment
                    },
                    success: function () {
                        window.location.reload();
                    },
                    error: function (request) {
                        var title = request.responseJSON.title ? request.responseJSON.title : 'Ada yang salah';
                        swal({
                            title: title,
                            html: request.responseJSON.message,
                            type: "warning"
                        });
                    }
                });
            }
        });

        return false;
    });
}