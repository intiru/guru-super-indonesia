<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;


Route::get('/error-test', function () {
    Log::info('calling the error route');
    throw new \Exception('something unexpected broke');
});

Route::get('/maintenance', function () {
    Artisan::call('down');
});

Route::get('/production', function () {
    Artisan::call('up');
});

Route::get('/encrypt/{id}', function ($id) {
    $hashids = new \Hashids\Hashids();
    return $hashids->encode([1, 2, 3, 4, 3, 2, 1]);
});

Route::get('/decrypt/{id}', function ($id) {
    $hashids = new \Hashids\Hashids();
    return $hashids->decode($id);
});


/**
 * User Page
 */

Route::group(['namespace' => 'Front'], function () {

//    $blog_category = \app\Models\mBlogCategory::get(['blc_judul','id_blog_category']);
//    $blog = \app\Models\mBlog::get(['id_blog', 'blg_judul']);

    /**
     * Website End User
     */
    Route::get('/', "Front@home")->name('frontBeranda');
    Route::get('/layanan-kami', "Front@layanan")->name('frontLayanan');
    Route::get('/tentang-kami', "Front@tentang")->name('frontTentang');

    Route::get('/blog', "Blog@index")->name('frontBlogList');
//    foreach ($blog_category as $row) {
//        Route::get('/blog/' . str_slug($row->blc_judul), function () use ($row) {
//            return \app\Http\Controllers\Front\Blog::blog_category($row->id_blog_category);
//        });
//    }
//    foreach($blog as $row) {
//        Route::get('/blog/'.str_slug($row->blg_judul), function() use ($row) {
//            return \app\Http\Controllers\Front\Blog::blog_detail($row->id_blog);
//        });
//    }

    Route::get('/faq', "Front@faq")->name('frontFaq');
    Route::get('/kontak-kami', "Front@kontak")->name('frontKontak');

    /**
     * Guru Account
     */
    Route::post('/register', 'Account@register_process')->name('registerProcess');
    Route::post('/login', 'Account@login_process')->name('loginProcess');
    Route::post('/reset-password', 'Account@reset_password_send_email')->name('resetPasswordSendEmail');
    Route::get('/reset-password/{id_guru}', 'Account@reset_password_page')->name('resetPasswordPage');
    Route::post('/reset-password/{id_guru}', 'Account@reset_password_process')->name('resetPasswordProcess');

    Route::get('/login/google', 'Account@login_google')->name('loginGoogle');
    Route::get('/login/google/redirect', 'Account@redirect_google')->name('redirectGoogle');
    Route::get('/login/facebook', 'Account@login_facebook')->name('loginFacebook');
    Route::get('/login/facebook/redirect', 'Account@redirect_facebook')->name('redirectFacebook');

    Route::group(['middleware' => 'checkLoginGuru'], function () {

        Route::get('/verifikasi/{id_guru}', 'Account@register_verifikasi')->name('registerVerifikasi');
    });

    Route::group(['middleware' => 'authLoginGuru'], function () {
        Route::get('/logout', 'Account@logout_process')->name('logoutProcess');
        Route::get('/profil-saya', "Account@profil")->name('accountProfil');
        Route::post('/profil-saya/update', "Account@profil_update")->name('accountProfilUpdate');
        Route::get('/profil-saya/step/2', "Account@profil_lengkapi_2")->name('accountProfilLengkapi2');
        Route::get('/profil-saya/step/3', "Account@profil_lengkapi_3")->name('accountProfilLengkapi3');
    });

    /**
     * Forum Diskusi
     */
    Route::get('/forum-diskusi', "Forum@list")->name('forumList');
    Route::group(['middleware' => 'authLoginGuru'], function () {

        Route::post('/forum-diskusi/guru/feed/like', "Forum@guru_feed_like")->name('forumGuruFeedLike');
        Route::delete('/forum-diskusi/guru/feed/delete', "Forum@guru_feed_delete")->name('forumGuruFeedDelete');
        Route::post('/forum-diskusi/guru/feed/edit/modal', "Forum@guru_feed_edit_modal")->name('forumGuruFeedEditModal');
        Route::post('/forum-diskusi/guru/feed/update/{id_feed}', "Forum@guru_feed_update")->name('forumGuruFeedUpdate');

        Route::post('/forum-diskusi/guru/comment', "Forum@guru_comment")->name('forumGuruComment');
        Route::post('/forum-diskusi/guru/comment/update', "Forum@guru_comment_update")->name('forumGuruCommentUpdate');
        Route::delete('/forum-diskusi/guru/comment/delete', "Forum@guru_comment_delete")->name('forumGuruCommentDelete');
        Route::post('/forum-diskusi/guru/feed', "Forum@guru_feed")->name('forumGuruFeedCreate');

        Route::get('/feed-saya', "Forum@feed_saya")->name('feedSayaList');
    });

});


/**
 * Back Office
 */

Route::group(['namespace' => 'BackOffice', 'middleware' => 'authLogin', 'prefix' => 'admin'], function () {

    /**
     * Feed Admin
     */
    Route::get('/feed-admin', 'FeedAdmin@index')->name('backFeedAdminList');
    Route::post('/feed-admin', 'FeedAdmin@insert')->name('backFeedAdminPost');
    Route::delete('/feed-admin/{id_guru}', 'FeedAdmin@delete')->name('backFeedAdminDelete');
    Route::get('/feed-admin/status/{id_feed}/{status}', 'FeedAdmin@status_update')->name('backFeedAdminStatus');
    Route::get('/feed-admin/modal/{id_feed}', 'FeedAdmin@edit_modal')->name('backFeedAdminEditModal');
    Route::post('/feed-admin/update/{id_feed}', 'FeedAdmin@update')->name('backFeedAdminUpdate');

    /**
     * Data Guru
     */
    Route::get('/guru', 'Guru@index')->name('backGuruList');

    /**
     * Kategori Blog
     */
    Route::get('/kategori-blog', "BlogCategory@index")->name('blogCategoryList');
    Route::post('/kategori-blog', "BlogCategory@insert")->name('blogCategoryInsert');
    Route::delete('/kategori-blog/{kode}', "BlogCategory@delete")->name('blogCategoryDelete');
    Route::get('/kategori-blog/modal/{kode}', "BlogCategory@edit_modal")->name('blogCategoryEditModal');
    Route::post('/kategori-blog/{kode}', "BlogCategory@update")->name('blogCategoryUpdate');

    /**
     * Blog
     */
    Route::get('/blog', "Blog@index")->name('blogList');
    Route::post('/blog', "Blog@insert")->name('blogInsert');
    Route::delete('/blog/{kode}', "Blog@delete")->name('blogDelete');
    Route::get('/blog/modal/{kode}', "Blog@edit_modal")->name('blogEditModal');
    Route::post('/blog/{kode}', "Blog@update")->name('blogUpdate');

});


Route::group(['namespace' => 'ReminderSetting', 'middleware' => 'authLogin', 'prefix' => 'admin'], function () {

    Route::get('/reminder/setting', 'ReminderSetting@index')->name('reminderSettingList');
    Route::post('/reminder/setting/update', 'ReminderSetting@update')->name('reminderSettingUpdate');

});

Route::group(['namespace' => 'ReminderHistory', 'middleware' => 'authLogin', 'prefix' => 'admin'], function () {

    Route::get('/reminder/history', 'ReminderHistory@index')->name('reminderHistoryList');
    Route::post('/reminder/history/datatable', 'ReminderHistory@data_table')->name('reminderHistoryDataTable');
    Route::get('/reminder/history/detail/{id_reminder_history}', 'ReminderHistory@detail_modal')->name('reminderHistoryDetail');

});

Route::group(['namespace' => 'ReminderMessage', 'middleware' => 'authLogin', 'prefix' => 'admin'], function () {

    Route::get('/reminder/message', "ReminderMessage@index")->name('reminderMessageList');
    Route::post('/reminder/message', "ReminderMessage@insert")->name('reminderMessageInsert');
    Route::get('/reminder/message/{id_reminder_message}', "ReminderMessage@edit_modal")->name('reminderMessageEditModal');
    Route::post('/reminder/message/{id_reminder_message}', "ReminderMessage@update")->name('reminderMessageUpdate');

});

Route::group(['namespace' => 'WhatsAppLink', 'middleware' => 'authLogin', 'prefix' => 'admin'], function () {

    Route::get('/whatsapp/link', 'WhatsAppLink@index')->name('whatsAppLink');

});

Route::group(['namespace' => 'ShceduleCalendar', 'middleware' => 'authLogin', 'prefix' => 'admin'], function () {

    Route::get('/schedule/calendar', 'ShceduleCalendar@index')->name('scheduleCalendarPage');
    Route::get('/schedule/modal', 'ShceduleCalendar@modal')->name('scheduleCalendarModal');

});

Route::group(['namespace' => 'General', 'prefix' => 'admin'], function () {

    Route::get('undercosntruction', "Underconstruction@index")->name('underconstructionPage');
    Route::post('city/list', "General@city_list")->name('cityList');
    Route::post('subdistrict/list', "General@subdistrict_list")->name('subdistrictList');

    Route::group(['middleware' => 'checkLogin'], function () {
        Route::get('/', "Login@index")->name("loginPage");
        Route::post('/roles/login', 'Login@roles')->name("rolesLogin");
        Route::post('/login', "Login@do")->name("loginDo");
    });

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/dashboard', "Dashboard@index")->name('dashboardPage');
        Route::get('/whatsapp-test', "Dashboard@whatsapp_test")->name('whatsappTest');
        Route::get('/logout', "Logout@do")->name('logoutDo');
        Route::get('/profile', "Profile@index")->name('profilPage');
        Route::post('/profile/administrator', "Profile@update_administrator")->name('profilAdministratorUpdate');

    });
});

// =============== Master Data =================== //

Route::group(['namespace' => 'MasterData', 'middleware' => 'authLogin', 'prefix' => 'admin'], function () {

    // Karyawan
    Route::get('/karyawan', "Karyawan@index")->name('karyawanPage');
    Route::post('/karyawan', "Karyawan@insert")->name('karyawanInsert');
    Route::get('/karyawan-list', "Karyawan@list")->name('karyawanList');
    Route::delete('/karyawan/{id}', "Karyawan@delete")->name('karyawanDelete');
    Route::get('/karyawan/{id}', "Karyawan@edit_modal")->name('karyawanEditModal');
    Route::post('/karyawan/{id}', "Karyawan@update")->name('karyawanUpdate');

    // User
    Route::get('/user', "User@index")->name('userPage');
    Route::post('/user', "User@insert")->name('userInsert');
    Route::get('/user-list', "User@list")->name('userList');
    Route::delete('/user/{kode}', "User@delete")->name('userDelete');
    Route::get('/user/modal/{kode}', "User@edit_modal")->name('userEditModal');
    Route::post('/user/{kode}', "User@update")->name('userUpdate');

    // Role User
    Route::get('/user-role', "UserRole@index")->name('userRolePage');
    Route::post('/user-role', "UserRole@insert")->name('userRoleInsert');
    Route::delete('/user-role/{id}', "UserRole@delete")->name('userRoleDelete');
    Route::get('/user-role/modal/{id}', "UserRole@edit")->name('userRoleEditModal');
    Route::post('/user-role/{id}', "UserRole@update")->name('userRoleUpdate');
    Route::get('/user-role/akses/{id}', "UserRole@akses")->name('userRoleAkses');
    Route::post('/user-role/akses/{id}', "UserRole@akses_update")->name('userRoleAksesUpdate');

    // Metode Tindakan
    Route::get('/metode-tindakan', "ActionMethod@index")->name('actionMethodPage');
    Route::post('/metode-tindakan', "ActionMethod@insert")->name('actionMethodInsert');
    Route::get('/metode-tindakan-list', "ActionMethod@list")->name('actionMethodList');
    Route::delete('/metode-tindakan/{kode}', "ActionMethod@delete")->name('actionMethodDelete');
    Route::get('/metode-tindakan/modal/{kode}', "ActionMethod@edit_modal")->name('actionMethodEditModal');
    Route::post('/metode-tindakan/{kode}', "ActionMethod@update")->name('actionMethodUpdate');

});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    //Artisan::call('route:cache');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

